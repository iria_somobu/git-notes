# Git Notes

Simple note taking app based on Git (as sync mechanism) and Markdown (as primary file format).

![](https://gitlab.com/iria_somobu/what-ive-done/-/raw/master/imgs/git_notes.png)
<div align="center"><figcaption>Android app interface</figcaption></div>

## Download

**Android**:
- Dev/nightly: you can download latest artifact [from Gitlab CI][droid_ci];
- Release: you can download stable (release) app [from Google Play][droid_gp].

[droid_ci]: https://gitlab.com/iria_somobu/git-notes/-/jobs/artifacts/master/file/git-notes.apk?job=build
[droid_gp]: https://play.google.com/store/apps/details?id=com.somobu.gitdesktop

