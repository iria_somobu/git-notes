# Linux Bash script

## Usage:
1. Adjust script variables in `git_desktop.sh`;
2. Edit crontab by `crontab - e`;
3. Make script run every 10 minutes by adding `*/10 * * * * /path/to/script/git_desktop.sh` to cron tab;
4. Exit from editor saving changes.
