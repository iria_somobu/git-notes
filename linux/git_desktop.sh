#!/bin/bash

# This settings you have to adjust by yourself:
# =============================================

export HOME="/home/somobu"

# This commands makes possible SSH login when script is executed by cron
# See: https://stackoverflow.com/a/48050119
export GIT_SSH_COMMAND="ssh -i $HOME/.ssh/gitlab_somobu"

# Path to the repo to sync
DESKTOP="/home/somobu/Desktop"


# This settings are fine by default
# =================================

# Log file name
LOG_FILE='git_desktop.log'

# Temp log file directory
TMP_LOG="/tmp/$LOG_FILE"


# This is script itself
# =====================

# Store initial directory
INIT_DIR=$(pwd)


# Format commit message
HOST=$(hostname)
printf -v DATE '%(%Y.%m.%d %H:%M)T' -1
COMMIT_MESSAGE="$DATE $HOST"


# Check that previous run was successful
cd $DESKTOP
if [ -f $LOG_FILE ]; then
  echo "$DATE Found log file on desktop, skipping" >> $LOG_FILE
  exit 1
fi


# Do a regular commit-pull-push
git add -A > $TMP_LOG
git commit -m "$COMMIT_MESSAGE" >> $TMP_LOG 2>&1
git pull >> $TMP_LOG 2>&1
rz=$?
# 1 is for 'pulled'?
if [[ $rz -ne 0 && $rz -ne 1 ]]; then
  echo "Rz fail: $rz" >> $TMP_LOG
  mv $TMP_LOG $DESKTOP
  exit 1
fi

git push >> $TMP_LOG 2>&1
rz=$?
#   0 is for 'nothing to push'?
# 128 is for 'push success'?
if [[ $rz -ne 0 && $rz -ne 128 ]]; then
  echo "Rz fail: $rz" >> $TMP_LOG
  mv $TMP_LOG $DESKTOP
  exit 1
fi


# If everything is fine, goto desktop
cd $INIT_DIR


