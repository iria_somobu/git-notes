# Privacy

## Effective date: August 05, 2022

This privacy policy is designed to provide transparency to your use of the GitNotes applications. This application allows you to write and manage notes.


## User provided information

If you request information or technical support, your personal data (such as your email address) is used only to respond to specific inquiry.


## Information gathering and usage

No information gathering and/or usage performed.


## Your data

Your data can be synchronized with other Git Hosting providers. We have no control over these 3rd party Git Hosting providers. We do not collect any information about your notes.


## Advertising

No advertising performed.


## Policy changes

This privacy policy may be updated from time to time for any reason. We will notify you of any changes to GitNotes Privacy Policy by posting the new Privacy Policy.
