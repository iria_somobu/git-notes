package com.somobu.gitdesktop.hierarchy

import android.app.AlertDialog
import android.content.Context
import android.os.Handler
import android.preference.PreferenceManager
import android.text.InputType
import android.widget.EditText
import android.widget.LinearLayout
import com.somobu.gitdesktop.GitOps
import com.somobu.gitdesktop.GitOpsService
import org.eclipse.jgit.api.ResetCommand
import org.eclipse.jgit.api.Status
import org.eclipse.jgit.lib.Constants
import org.eclipse.jgit.lib.ObjectId

object Dialogs {

    fun manualCommit(ctx: Context) {
        val name = EditText(ctx)
        name.hint = "Commit message"

        AlertDialog.Builder(ctx)
            .setTitle("Manual commit")
            .setView(name)
            .setNegativeButton("Cancel", null)
            .setPositiveButton("Commit", { _, _ ->
                run { GitOpsService.commit(ctx, name.text.toString()) }
            })
            .show()
    }

    fun setupAutocommit(ctx: Context) {
        AlertDialog.Builder(ctx)
            .setTitle("Auto commit settings")
            .setMessage("Unimplemented yet")
            .setPositiveButton("Yes", null)
            .show()
    }

    fun setupGitIdent(ctx: Context) {
        val prefs = PreferenceManager.getDefaultSharedPreferences(ctx)

        val layout = LinearLayout(ctx)
        layout.orientation = LinearLayout.VERTICAL

        val name = EditText(ctx)
        name.hint = "Git user name"
        name.setText(prefs.getString("GIT_USER", null))
        layout.addView(name)

        val mail = EditText(ctx)
        mail.hint = "Git user email"
        mail.setText(prefs.getString("GIT_MAIL", null))
        layout.addView(mail)

        AlertDialog.Builder(ctx)
            .setTitle("Git ident")
            .setView(layout)
            .setNegativeButton("Cancel", null)
            .setPositiveButton("Confirm", { _, _ ->
                run {
                    var git_user = name.text.toString().trim()
                    var git_mail = mail.text.toString().trim()

                    if (git_user.isEmpty()) git_user = "root"
                    if (git_mail.isEmpty()) git_mail = "root@localhost"

                    PreferenceManager.getDefaultSharedPreferences(ctx)
                        .edit()
                        .putString("GIT_USER", git_user)
                        .putString("GIT_MAIL", git_mail)
                        .apply()
                }
            })
            .show()
    }

    fun setupRepoCredentials(ctx: Context) {
        val prefs = PreferenceManager.getDefaultSharedPreferences(ctx)

        val layout = LinearLayout(ctx)
        layout.orientation = LinearLayout.VERTICAL

        val name = EditText(ctx)
        name.hint = "Repo username"
        name.setText(prefs.getString("REPO_USER", null))
        layout.addView(name)

        val pwd = EditText(ctx)
        pwd.hint = "Repo password"
        pwd.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        layout.addView(pwd)

        AlertDialog.Builder(ctx)
            .setTitle("Repo credentials")
            .setView(layout)
            .setNegativeButton("Cancel", null)
            .setPositiveButton("Confirm", { _, _ ->
                run {
                    val repo_user = name.text.toString().trim()
                    val repo_pwd = pwd.text.toString().trim()

                    PreferenceManager.getDefaultSharedPreferences(ctx)
                        .edit()
                        .putString("REPO_USER", repo_user)
                        .putString("REPO_PWD", repo_pwd)
                        .apply()
                }
            })
            .show()
    }

    fun gitHistory(ctx: Context) {
        val ab = AlertDialog.Builder(ctx)
            .setMessage("Reading git history")
            .setCancelable(false)
            .create()
        ab.show()

        val handler = Handler()
        val work = object : GitOps.RepoWork() {

            override fun run() {
                val head: ObjectId? = thread.git.repository.resolve(Constants.HEAD)

                var msg = ""
                if (head == null) {
                    msg = "No commits yet"
                } else {
                    val commits = thread.git.log().add(head).setMaxCount(10).call()
                    for (commit in commits) {
                        msg += commit.shortMessage + "\n"
                    }
                }

                handler.post {
                    ab.hide()

                    AlertDialog.Builder(ctx)
                        .setTitle("Last 10 commits")
                        .setMessage(msg)
                        .setPositiveButton("Ok", null)
                        .show()
                }
            }

        }

        val thread = GitOps.RepoWorkerThread(ctx, work)
        thread.start()
    }

    fun gitUncommitted(ctx: Context) {
        val ab = AlertDialog.Builder(ctx)
            .setMessage("Reading git history")
            .setCancelable(false)
            .create()
        ab.show()

        val handler = Handler()
        val work = object : GitOps.RepoWork() {

            override fun run() {
                val status: Status = thread.git.status().call();
                val changes = status.uncommittedChanges;

                var msg = ""
                if (changes.isEmpty()) {
                    msg = "No changes"
                } else {
                    for (change in changes) {
                        msg += change + "\n"
                    }
                }

                handler.post {
                    ab.hide()

                    AlertDialog.Builder(ctx)
                        .setTitle("Uncommitted changes")
                        .setMessage(msg)
                        .setPositiveButton("Ok", null)
                        .show()
                }
            }

        }

        val thread = GitOps.RepoWorkerThread(ctx, work)
        thread.start()
    }

    fun changeUpstream(ctx: Context) {

        val ab = AlertDialog.Builder(ctx)
            .setMessage("Reading git properties")
            .setCancelable(false)
            .create()
        ab.show()

        val handler = Handler()
        val work = object : GitOps.RepoWork() {

            override fun run() {
                val url = thread.git.repository.config.getString("remote", "origin", "url")

                handler.post {
                    ab.hide()
                    changeUpstream(ctx, url)
                }
            }

        }

        val thread = GitOps.RepoWorkerThread(ctx, work)
        thread.start()
    }

    fun changeUpstream(ctx: Context, url: String?) {

        val layout = LinearLayout(ctx)
        layout.orientation = LinearLayout.VERTICAL

        val name = EditText(ctx)
        name.hint = "Upstream url"
        name.setText(url)
        layout.addView(name)

        AlertDialog.Builder(ctx)
            .setTitle("Upstream url")
            .setView(layout)
            .setNegativeButton("Cancel", null)
            .setPositiveButton("Ok", { _, _ ->
                run {
                    val work = object : GitOps.RepoWork() {
                        override fun run() {
                            thread.git.repository.config.setString(
                                "remote", "origin", "url",
                                name.text.toString()
                            )
                            thread.git.repository.config.save()
                        }
                    }

                    val thread = GitOps.RepoWorkerThread(ctx, work)
                    thread.start()
                }
            })
            .show()

    }

    fun forcePull(ctx: Context) {
        val ab = AlertDialog.Builder(ctx)
            .setMessage("Fetching")
            .setCancelable(false)
            .create()
        ab.show()

        val handler = Handler()
        val work = object : GitOps.RepoWork() {

            override fun run() {
                thread.git.fetch().setRemote("origin").setCredentialsProvider(thread.auth).call();
                thread.git.reset().setMode(ResetCommand.ResetType.HARD).setRef("origin/master").call();

                handler.post {
                    ab.hide()

                    AlertDialog.Builder(ctx)
                        .setTitle("Force pull")
                        .setMessage("Done!")
                        .setPositiveButton("Ok", null)
                        .show()
                }
            }

        }

        val thread = GitOps.RepoWorkerThread(ctx, work)
        thread.start()
    }
}