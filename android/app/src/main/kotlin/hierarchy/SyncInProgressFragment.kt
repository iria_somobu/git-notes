@file:Suppress("DEPRECATION", "OVERRIDE_DEPRECATION")

package com.somobu.gitdesktop.hierarchy

import android.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.somobu.gitdesktop.R

class SyncInProgressFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_sync_in_progress, container, false)
    }

}