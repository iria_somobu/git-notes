package com.somobu.gitdesktop.hierarchy

import android.view.View
import android.widget.AbsListView


/**
 * Created by mariotaku on 22.10.2014 14/10/22.
 */
class ListScrollDistanceCalculator(private val listener: Listener) : AbsListView.OnScrollListener {

    private var mListScrollStarted = false
    private var mFirstVisibleItem = 0
    private var mFirstVisibleHeight = 0
    private var mFirstVisibleTop = 0
    private var mFirstVisibleBottom = 0
    var totalScrollDistance = 0
        private set

    override fun onScrollStateChanged(view: AbsListView, scrollState: Int) {
        if (view.count == 0) return
        when (scrollState) {
            AbsListView.OnScrollListener.SCROLL_STATE_IDLE -> {
                mListScrollStarted = false
            }
            AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL -> {
                val firstChild: View = view.getChildAt(0)
                mFirstVisibleItem = view.firstVisiblePosition
                mFirstVisibleTop = firstChild.getTop()
                mFirstVisibleBottom = firstChild.getBottom()
                mFirstVisibleHeight = firstChild.getHeight()
                mListScrollStarted = true
                totalScrollDistance = 0
            }
        }
    }

    override fun onScroll(view: AbsListView, firstVisibleItem: Int, visibleItemCount: Int, totalItemCount: Int) {
        if (totalItemCount == 0 || !mListScrollStarted) return
        val firstChild: View = view.getChildAt(0)
        val firstVisibleTop: Int = firstChild.getTop()
        val firstVisibleBottom: Int = firstChild.getBottom()
        val firstVisibleHeight: Int = firstChild.getHeight()
        val delta: Int
        if (firstVisibleItem > mFirstVisibleItem) {
            mFirstVisibleTop += mFirstVisibleHeight
            delta = firstVisibleTop - mFirstVisibleTop
        } else if (firstVisibleItem < mFirstVisibleItem) {
            mFirstVisibleBottom -= mFirstVisibleHeight
            delta = firstVisibleBottom - mFirstVisibleBottom
        } else {
            delta = firstVisibleBottom - mFirstVisibleBottom
        }
        totalScrollDistance += delta

        listener.onScrollDistanceChanged(delta, totalScrollDistance)

        mFirstVisibleTop = firstVisibleTop
        mFirstVisibleBottom = firstVisibleBottom
        mFirstVisibleHeight = firstVisibleHeight
        mFirstVisibleItem = firstVisibleItem
    }

    interface Listener {
        fun onScrollDistanceChanged(delta: Int, total: Int)
    }
}
