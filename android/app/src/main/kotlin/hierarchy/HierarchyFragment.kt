@file:Suppress("OVERRIDE_DEPRECATION", "DEPRECATION")

package com.somobu.gitdesktop.hierarchy

import android.app.ListFragment
import android.os.Bundle
import android.view.ContextMenu
import android.view.MenuItem
import android.view.View
import android.widget.AbsListView
import android.widget.AdapterView.AdapterContextMenuInfo
import android.widget.ListView
import com.somobu.essentials.CustomizedActivity
import com.somobu.gitdesktop.Bugs
import com.somobu.gitdesktop.R
import java.io.File


class HierarchyFragment : ListFragment() {

    private enum class Actions {
        RENAME,
        DELETE,
        MOVE,
        COPY
    }

    private var files = emptyArray<File?>()
    private var selected = mutableListOf<File>()

    var isSelectionMode = false
    val selectedFiles = arrayListOf<File>()

    fun ops(): HierarchyActivity.HierarchyOperations? {
        return (activity as HierarchyActivity?)?.hierarchyOperations
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        registerForContextMenu(listView)

        if (savedInstanceState != null) {
            val list = savedInstanceState.getStringArrayList("SELECTED")!!
            for (f in list) {
                selectedFiles.clear()
                selectedFiles.add(File(f))
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Bugs.preventInfiniteListLoading(this)

        val listAdapter = object : FileAdapter(view.context) {

            override fun onSelectionChanged(file: File, isSelected: Boolean) {
                if (isSelected) selectedFiles.add(file)
                else selectedFiles.remove(file)

                ops()?.onSelectionChange(selectedFiles)
            }
        }
        listAdapter.setItems(files, selected)
        setListAdapter(listAdapter)

        listView.setOnScrollListener(ListScrollDistanceCalculator(object : ListScrollDistanceCalculator.Listener {

            override fun onScrollDistanceChanged(delta: Int, total: Int) {
                val maActivity = this@HierarchyFragment.activity as CustomizedActivity?
                maActivity?.handleScrollY(delta.toFloat())
            }

        }))

        val padderTop = View(view.context)
        val padderTopHeight = view.context.resources.getDimension(com.somobu.essentials.R.dimen.header_height).toInt()
        padderTop.layoutParams = AbsListView.LayoutParams(0, padderTopHeight)
        listView.addHeaderView(padderTop)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)

        val list = mutableListOf<String>()
        for (f in selectedFiles) list.add(f.absolutePath)
        outState!!.putStringArrayList("SELECTED", ArrayList(list))
    }

    override fun onCreateContextMenu(
        menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        if (v!!.id == android.R.id.list) {
            val info = menuInfo as AdapterContextMenuInfo
            val item = (v as ListView).getItemAtPosition(info.position) as File

            menu!!.setHeaderTitle(item.name)
            menu.add(info.position, Actions.RENAME.ordinal, 0, "Rename")
            menu.add(info.position, Actions.DELETE.ordinal, 20, "Delete")
            menu.add(info.position, Actions.MOVE.ordinal, 21, "Move")
            menu.add(info.position, Actions.COPY.ordinal, 22, "Copy")
        } else {
            super.onCreateContextMenu(menu, v, menuInfo)
        }
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        val info = item.menuInfo as AdapterContextMenuInfo
        if (listView.getPositionForView(info.targetView) == -1) {
            return super.onContextItemSelected(item)
        } else {
            val file: File = listView.getItemAtPosition(item.groupId) as File
            when (item.itemId) {
                Actions.RENAME.ordinal -> ops()?.fileRename(file)
                Actions.DELETE.ordinal -> ops()?.fileDelete(file)
                Actions.MOVE.ordinal -> {
                    selectedFiles.clear()
                    selectedFiles.add(file)
                    ops()?.onSelectionChange(selectedFiles)
                    ops()?.bulkMove()
                }

                Actions.COPY.ordinal -> {
                    selectedFiles.clear()
                    selectedFiles.add(file)
                    ops()?.onSelectionChange(selectedFiles)
                    ops()?.bulkCopy()
                }
            }
            return true
        }
    }

    override fun onListItemClick(l: ListView, v: View, position: Int, id: Long) {
        val headers = listView.headerViewsCount
        if (position < headers) return

        val file = (listAdapter as FileAdapter).getItem(position - headers)!!
        if (isSelectionMode) {
            if (file in selectedFiles) selectedFiles.remove(file)
            else selectedFiles.add(file)

            ops()?.onSelectionChange(selectedFiles)
        } else {
            ops()?.onFileClick(file)
        }
    }

    fun getSelected(): List<File> {
        return selectedFiles
    }

    fun setItems(items: Array<File?>, selected: MutableList<File>) {
        this.files = items
        this.selected = selected
        (listAdapter as FileAdapter?)?.setItems(items, selected)
    }
}