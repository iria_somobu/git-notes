@file:Suppress("DEPRECATION")

package com.somobu.gitdesktop.hierarchy

import GitO
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.*
import android.net.Uri
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.View
import android.widget.EditText
import android.widget.Toast
import com.somobu.essentials.CustomizedActivity
import com.somobu.essentials.FileChooser
import com.somobu.gitdesktop.*
import com.somobu.gitdesktop.sync.SyncHelper
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class HierarchyActivity : CustomizedActivity() {

    private lateinit var gitEventsReceiver: BroadcastReceiver

    val hierarchyOperations = HierarchyOperations()

    private val hierarchyFragment = HierarchyFragment()
    private val noFilesFragment = NoFilesFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHeaderColor(resources.getColor(com.somobu.essentials.R.color.green))
        setHeaderTitle("[Root]")

        hierarchyOperations.defaultDir = GitO.getRepoDir(this)

        registerReceivers()

        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        val hasInit = prefs.getBoolean("INIT_DONE", false)

        if (hasInit) {
            SyncHelper.scheduleSync(this)

            // We don't have to schedule sync if no sync was performed yet
            // -- 'we have no sync' means that user just moved from setup screen and sync just not ended yet
            // if (!hasNoSync()) SyncHelper.requestSync(this)

            val filename = intent.getStringExtra("file") ?: GitO.getRepoDir(this).canonicalPath
            val file = File(filename)
            hierarchyOperations.showDir(file)

            val time = prefs.getString("SYNC_TIME_STAT", "N/A")
            setHeaderSubtitle("Last sync: $time")
        } else {
            startActivity(Intent(this, SetupActivity::class.java))
            finish()
        }
    }

    override fun getBottomButtons(): Array<Btn> {
        return arrayOf(
            // Default
            object : Btn() {
                override fun getGroup() = GROUP_DEFAULT
                override fun getIconRes() = R.drawable.ic_checklist
                override fun getLabelRes() = R.string.btn_select_all
                override fun click(): (View) -> Unit = { hierarchyOperations.selectAll() }
            },
            object : Btn() {
                override fun getGroup() = GROUP_DEFAULT
                override fun getIconRes() = R.drawable.ic_new_file
                override fun getLabelRes() = R.string.btn_new_file
                override fun click(): (View) -> Unit = { newFile() }
            },
            object : Btn() {
                override fun getGroup() = GROUP_DEFAULT
                override fun getIconRes() = R.drawable.ic_new_folder
                override fun getLabelRes() = R.string.btn_new_folder
                override fun click(): (View) -> Unit = { newFolder() }
            },

            // Selected
            object : Btn() {
                override fun getGroup() = GROUP_SELECTION
                override fun getIconRes() = R.drawable.ic_checklist
                override fun getLabelRes() = R.string.btn_deselect_all
                override fun click(): (View) -> Unit = { deselectAll() }
            },
            object : Btn() {
                override fun getGroup() = GROUP_SELECTION
                override fun getIconRes() = R.drawable.ic_copy
                override fun getLabelRes() = R.string.btn_bulk_copy
                override fun click(): (View) -> Unit = { hierarchyOperations.bulkCopy() }
            },
            object : Btn() {
                override fun getGroup() = GROUP_SELECTION
                override fun getIconRes() = R.drawable.ic_move
                override fun getLabelRes() = R.string.btn_bulk_move
                override fun click(): (View) -> Unit = { hierarchyOperations.bulkMove() }
            },
            object : Btn() {
                override fun getGroup() = GROUP_SELECTION
                override fun getIconRes() = R.drawable.ic_delete
                override fun getLabelRes() = R.string.btn_bulk_delete
                override fun click(): (View) -> Unit = { bulkDelete() }
            },

            // Always
            object : Btn() {
                override fun getGroup() = GROUP_ALWAYS
                override fun getIconRes() = R.drawable.ic_git_sync
                override fun getLabelRes() = R.string.btn_git_sync
                override fun click(): (View) -> Unit = {
                    val items = arrayOf(
                        "Trigger sync",
                        "Manual commit",
                        "x_x Auto-commit settings",
                        "Change git ident",
                        "Change repo credentials",
                        "Change upstream",
                        "View uncommitted changes",
                        "Force pull from origin/master"
                    )

                    AlertDialog.Builder(this@HierarchyActivity)
                        .setTitle("Git actions")
                        .setItems(items, { _, i ->
                            run {
                                when (i) {
                                    0 -> GitOpsService.sync(this@HierarchyActivity)
                                    1 -> Dialogs.manualCommit(this@HierarchyActivity)
                                    2 -> Dialogs.setupAutocommit(this@HierarchyActivity)
                                    3 -> Dialogs.setupGitIdent(this@HierarchyActivity)
                                    4 -> Dialogs.setupRepoCredentials(this@HierarchyActivity)
                                    5 -> Dialogs.changeUpstream(this@HierarchyActivity)
                                    6 -> Dialogs.gitUncommitted(this@HierarchyActivity)
                                    7 -> Dialogs.forcePull(this@HierarchyActivity)
                                }
                            }
                        })
                        .show()
                }
            },
            object : Btn() {
                override fun getGroup() = GROUP_ALWAYS
                override fun getIconRes() = R.drawable.ic_history
                override fun getLabelRes() = R.string.btn_git_hist
                override fun click(): (View) -> Unit = { Dialogs.gitHistory(this@HierarchyActivity) }
            },
            object : Btn() {
                override fun getGroup() = GROUP_ALWAYS
                override fun getIconRes() = R.drawable.ic_help
                override fun getLabelRes() = R.string.users_manual
                override fun click(): (View) -> Unit = {
                    ManualActivity.start(this@HierarchyActivity, "Hierarchy_viewer")
                }
            },
        )
    }

    private fun registerReceivers() {

        gitEventsReceiver = object : BroadcastReceiver() {
            override fun onReceive(ctx: Context, intent: Intent) {
                // First of all, check that we have correct overlays
                ensureHelpOverlayState()

                when (intent.action) {
                    GitOps.BROADCAST_STATUS -> {
                        uiSyncStatus(intent.getStringExtra("message")!!)
                    }

                    GitOps.BROADCAST_ERROR -> {
                        val reason =
                            (intent.getSerializableExtra("error") as GitOps.SyncFail?)!!
                        resolveSyncFail(reason)
                    }

                    GitOps.BROADCAST_PULLED -> {
                        hierarchyOperations.reload()
                    }

                    GitOps.BROADCAST_CONFLICT -> {
                        uiSyncStatus(intent.getStringExtra("message")!!)
                    }

                    GitOps.BROADCAST_FINISHED -> {

                        // Sometimes shared prefs' last sync time may be missing for a while
                        // when committed from different process. Let's commit it here
                        GitOps.updateLastSyncTime(this@HierarchyActivity)

                        hierarchyOperations.reload()
                    }
                }

            }
        }

        val filter = IntentFilter()
        filter.addAction(GitOps.BROADCAST_STATUS)
        filter.addAction(GitOps.BROADCAST_ERROR)
        filter.addAction(GitOps.BROADCAST_PULLED)
        filter.addAction(GitOps.BROADCAST_CONFLICT)
        filter.addAction(GitOps.BROADCAST_FINISHED)
        registerReceiver(gitEventsReceiver, filter)
    }

    override fun onDestroy() {
        unregisterReceiver(gitEventsReceiver)
        super.onDestroy()
    }

    override fun onBackPressed() {
        var processed = false

        if (getCurrentFragment() is HierarchyFragment) {
            processed = hierarchyOperations.onBackPressed()
        }

        if (!processed) super.onBackPressed()
    }

    private fun hasNoSync(): Boolean {
        val sts = PreferenceManager.getDefaultSharedPreferences(this).getString("SYNC_TIME_STAT", null)
        println("STS $sts")
        return sts == null
    }

    fun ensureHelpOverlayState() {
        val files = hierarchyOperations.getCurrDir().listFiles() ?: emptyArray<File>()
        val filtered = files.filter { file -> !file.name.startsWith(".") }

        val hasNoFiles = filtered.isEmpty()

        val fragment = if (hasNoSync()) {
            SyncInProgressFragment()
        } else if (hasNoFiles) {
            val canGoUp = hierarchyOperations.canGoUp()
            noFilesFragment.setGoUp(canGoUp)
            noFilesFragment
        } else {
            hierarchyFragment
        }

        val currentFragment = getCurrentFragment()
        if (fragment != currentFragment) setFragment(fragment)
    }

    private fun onSelectionChange(files: List<File>) {
        switchMode(files.isNotEmpty())

        if (files.size == 1) setSelectionModeLabel("${files[0].name} selected")
        else setSelectionModeLabel("${files.size} files selected")
    }

    private fun onDirectoryChange(dirname: String) {
        setHeaderTitle("[Root]$dirname")
        setHomeButton(hierarchyOperations.canGoUp(), { hierarchyOperations.goUp() }, R.drawable.ic_arrow_up)
        ensureHelpOverlayState()
    }

    private fun reloadView() {
        ensureHelpOverlayState()
        hierarchyOperations.reload()
    }


    private fun viewFile(file: File) {

        val ext: String?

        val lastDotPtr = file.name.lastIndexOf(".")
        if (lastDotPtr == -1) ext = null
        else ext = file.name.substring(lastDotPtr + 1)

        val intent: Intent = when (ext) {
            "txt" -> Intent(this, com.somobu.viewtxt.TextEditorActivity::class.java)
            "md" -> Intent(this, com.somobu.viewtxt.TextEditorActivity::class.java)
            "dot" -> Intent(this, com.somobu.viewdot.DotEditorActivity::class.java)
            else -> viewExternalIntent(file)
        }
        intent.putExtra("file", file.canonicalPath)

        try {
            startActivity(intent)
        } catch (e: Exception) {
            Toast.makeText(this, "There's no app to open ${file.name}", Toast.LENGTH_SHORT)
                .show()
        }
    }

    private fun viewExternalIntent(file: File): Intent {
        val uri: Uri =
            FileProvider.getUriForFile(this, "com.somobu.gitdesktop.fileprovider", file)!!
        val mime = contentResolver.getType(uri)

        val intent = Intent()
        intent.action = Intent.ACTION_VIEW
        intent.setDataAndType(uri, mime)
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

        return intent
    }


    @SuppressLint("SimpleDateFormat")
    fun uiSyncStatus(status: String) {
        val date = SimpleDateFormat("HH:mm").format(Date(System.currentTimeMillis()))
        setHeaderSubtitle("$date $status")
    }

    fun resolveSyncFail(reason: GitOps.SyncFail) {
        when (reason) {

            GitOps.SyncFail.LOCKED_INDEX -> {
                AlertDialog.Builder(this)
                    .setMessage(".git/index is locked. Do you want to force unlock it?")
                    .setNegativeButton("No", null)
                    .setPositiveButton("Yes", { p0, p1 ->
                        run {
                            GitOps.forceDeleteIndexLock(this)
                            uiSyncStatus("Unlocked. You may now try again")
                        }
                    }).show()
            }

            GitOps.SyncFail.UNAUTHORIZED -> {
                AlertDialog.Builder(this)
                    .setTitle("Error: not authorized")
                    .setMessage(
                        "It seems that credentials you provided does not allow to interact" +
                                " with remote repo. Try change repo credentials in sync menu."
                    )
                    .setNegativeButton("Ok", null)
                    .show()
            }

            GitOps.SyncFail.NETWORK_ERROR -> {
                AlertDialog.Builder(this)
                    .setTitle("Unable connect to host")
                    .setMessage(
                        "It seems that we're unable connect to repo host. No internet or networking error?"
                    )
                    .setNegativeButton("Ok", null)
                    .show()
            }

            GitOps.SyncFail.INVALID_REMOTE -> {

                // We may get 'invalid remote' error when coming from setup screen
                // Let's set sync time here to show valid hierarchy fragment
                PreferenceManager.getDefaultSharedPreferences(this@HierarchyActivity)
                    .edit().putString("SYNC_TIME_STAT", "Error: invalid remote").apply()
                ensureHelpOverlayState()

                AlertDialog.Builder(this)
                    .setTitle("Invalid remote URL")
                    .setMessage(
                        "It seems that url you provided is wrong. Do you want to change it now?"
                    )
                    .setNegativeButton("No", null)
                    .setPositiveButton("Yes", { _: DialogInterface, _: Int ->
                        run {
                            Dialogs.changeUpstream(this@HierarchyActivity)
                        }
                    })
                    .show()
            }

            GitOps.SyncFail.TRANSPORT_ERROR -> {

                // We may get 'transport error' when coming from setup screen
                // Let's set sync time here to show valid hierarchy fragment
                PreferenceManager.getDefaultSharedPreferences(this@HierarchyActivity)
                    .edit().putString("SYNC_TIME_STAT", "Error: transport error").apply()
                ensureHelpOverlayState()

                AlertDialog.Builder(this)
                    .setTitle("Transport error")
                    .setMessage(
                        "Looks like we're unable to connect to repo. Please check that you have internet access, and " +
                                "repo url, username and password are valid."
                    )
                    .setNegativeButton("Ok", null)
                    .show()
            }
        }
    }

    fun deselectAll() = hierarchyOperations.deselectAll()

    fun newFile() {
        val edit = EditText(this)
        edit.setHint("filename.txt")

        AlertDialog.Builder(this)
            .setTitle("Create new file")
            .setView(edit)
            .setNegativeButton("Cancel", null)
            .setPositiveButton("Ok") { _: DialogInterface, _: Int ->
                run {
                    val file = File(hierarchyOperations.getCurrDir(), edit.text.toString())
                    var result = false
                    try {
                        result = file.createNewFile()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    reloadView()

                    if (result) viewFile(file)
                    else Toast.makeText(this, "Unable to create file!", Toast.LENGTH_LONG).show()
                }
            }
            .show()
    }

    fun newFolder() {
        val edit = EditText(this)
        edit.setHint("folder_name")

        AlertDialog.Builder(this)
            .setTitle("Create new folder")
            .setView(edit)
            .setNegativeButton("Cancel", null)
            .setPositiveButton("Ok") { _: DialogInterface, _: Int ->
                run {
                    val file = File(hierarchyOperations.getCurrDir(), edit.text.toString())

                    var result = false
                    try {
                        result = file.mkdirs()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    if (!result) Toast.makeText(this, "Unable to create folder!", Toast.LENGTH_LONG).show()
                    reloadView()
                }
            }
            .show()
    }

    fun bulkDelete() {
        AlertDialog.Builder(this)
            .setMessage("Do you really want delete selected files?")
            .setNegativeButton("No", null)
            .setPositiveButton("Yes") { _, _ ->
                run {
                    val ab = AlertDialog.Builder(this)
                        .setMessage("Removing")
                        .setCancelable(false)
                        .show()

                    val files = hierarchyFragment.getSelected()
                    var str = ""

                    for (file in files) str += " \"${file.absolutePath}\""

                    ShellHelper.execAsync(
                        "rm -rf $str",
                        this,
                        {
                            deselectAll()
                            reloadView()
                            ab.hide()
                        })
                }
            }
            .show()
    }

    inner class HierarchyOperations {

        private val hiddenFilter: (File) -> Boolean = { !it.name.startsWith(".") }

        var defaultDir: File? = null
        var currentDir: File? = null

        fun getCurrDir(): File = currentDir ?: GitO.getRepoDir(applicationContext)

        fun reload() = showDir(currentDir)

        fun canGoUp(): Boolean {
            val parentName = GitO.getRepoDir(applicationContext).canonicalPath + File.separator
            val longerName = currentDir!!.canonicalPath
            return longerName.startsWith(parentName)
        }

        fun goUp() {
            if (currentDir != null) showDir(currentDir?.parentFile)
            else showDir(GitO.getRepoDir(applicationContext))
        }

        fun showDir(dir: File?) {
            if (dir != null) currentDir = dir
            else if (currentDir == null) currentDir = defaultDir

            val isSubfolder = canGoUp()

            val shitLoad = currentDir!!.listFiles()!!.filter(hiddenFilter).toTypedArray()

            val selectedFiles = hierarchyFragment.selectedFiles
            val filteredSelected = mutableListOf<File>()
            for (file in selectedFiles) {
                if (file.parent == currentDir!!.absolutePath) filteredSelected.add(file)
            }

            hierarchyFragment.setItems(shitLoad, filteredSelected)

            if (!isSubfolder) onDirectoryChange("/")
            else onDirectoryChange(currentDir?.canonicalPath!!.substring(defaultDir!!.canonicalPath.length))
        }

        fun onSelectionChange(files: ArrayList<File>) {
            hierarchyFragment.isSelectionMode = files.isNotEmpty()
            this@HierarchyActivity.onSelectionChange(files)
        }

        fun onFileClick(file: File) {
            if (file.isDirectory) showDir(file)
            else viewFile(file)
        }

        fun selectAll() {
            hierarchyFragment.selectedFiles.clear()
            val selection = (currentDir ?: defaultDir!!).listFiles()!!.filter(hiddenFilter)
            hierarchyFragment.selectedFiles.addAll(selection)
            onSelectionChange(hierarchyFragment.selectedFiles)
            reload()
        }

        fun deselectAll() {
            hierarchyFragment.selectedFiles.clear()
            onSelectionChange(hierarchyFragment.selectedFiles)
            reload()
        }

        fun onBackPressed(): Boolean {

            if (currentDir != null && defaultDir != null) {
                val parentDir = currentDir?.parent
                if (parentDir != null && parentDir.startsWith(defaultDir!!.absolutePath)) {
                    showDir(File(parentDir))
                    return true
                }
            }

            return false
        }

        @Suppress("CascadeIf")
        fun fileRename(file: File) {
            val view = EditText(this@HierarchyActivity)
            view.setText(file.name)

            AlertDialog.Builder(this@HierarchyActivity)
                .setTitle("Rename ${file.name}")
                .setView(view)
                .setNegativeButton("Cancel", null)
                .setPositiveButton("Confirm") { _: DialogInterface, _: Int ->
                    run {
                        val text = view.text.toString().trim()
                        if (text.isEmpty()) {
                            Toast.makeText(this@HierarchyActivity, "Name cannot be empty", Toast.LENGTH_SHORT).show()
                        } else if (text.contains("/")) {
                            Toast.makeText(this@HierarchyActivity, "No slashes please", Toast.LENGTH_SHORT).show()
                        } else {
                            val newName = File(file.parentFile, text)
                            file.renameTo(newName)
                            reloadView()
                        }
                    }
                }
                .show()
        }

        fun fileDelete(file: File) {
            val type = (if (file.isDirectory) "folder" else "file")
            AlertDialog.Builder(this@HierarchyActivity)
                .setMessage("Delete $type ${file.name}?")
                .setNegativeButton("No", null)
                .setPositiveButton("Yes") { _: DialogInterface, _: Int ->
                    run {
                        val ab = AlertDialog.Builder(this@HierarchyActivity)
                            .setMessage("Removing")
                            .setCancelable(false)
                            .show()
                        ShellHelper.execAsync("rm -rf \"${file.absolutePath}\"", this@HierarchyActivity, {
                            reloadView()
                            ab.hide()
                        })
                    }
                }
                .show()
        }

        fun bulkCopy() {
            FileChooser.pickFolder(
                this@HierarchyActivity,
                "Copy to: ",
                hierarchyOperations.getCurrDir(),
                { file: File ->
                    run {
                        val ab = AlertDialog.Builder(this@HierarchyActivity)
                            .setMessage("Copying")
                            .setCancelable(false)
                            .show()

                        var str = ""

                        for (f in hierarchyFragment.getSelected()) str += " \"${f.canonicalPath}\""

                        ShellHelper.execAsync(
                            "cp -n $str \"${file.canonicalPath}\"",
                            this@HierarchyActivity,
                            {
                                deselectAll()
                                reloadView()
                                ab.hide()
                            })
                    }
                },
                {})
        }

        fun bulkMove() {
            FileChooser.pickFolder(
                this@HierarchyActivity,
                "Move to: ",
                hierarchyOperations.getCurrDir(),
                { file: File ->
                    run {
                        val ab = AlertDialog.Builder(this@HierarchyActivity)
                            .setMessage("Moving")
                            .setCancelable(false)
                            .show()

                        var str = ""

                        for (f in hierarchyFragment.getSelected()) str += " \"${f.canonicalPath}\""

                        ShellHelper.execAsync(
                            "mv -n $str \"${file.canonicalPath}\"",
                            this@HierarchyActivity,
                            {
                                deselectAll()
                                reloadView()
                                ab.hide()
                            })
                    }
                },
                {})
        }

    }
}