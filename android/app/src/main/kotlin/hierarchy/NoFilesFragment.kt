@file:Suppress("DEPRECATION", "OVERRIDE_DEPRECATION")

package com.somobu.gitdesktop.hierarchy

import android.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.TextView
import com.somobu.gitdesktop.R

class NoFilesFragment : Fragment() {

    private var canGoUp = true

    fun setGoUp(goUp: Boolean) {
        canGoUp = goUp
        view?.findViewById<LinearLayout>(R.id.item_root)?.visibility = if (goUp) View.VISIBLE else View.GONE
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_no_files, container, false)

        root.findViewById<ImageButton>(R.id.hint_btn_file).setOnClickListener {
            val a = this@NoFilesFragment.activity as HierarchyActivity?
            a?.newFile()
        }
        root.findViewById<ImageButton>(R.id.hint_btn_folder).setOnClickListener {
            val a = this@NoFilesFragment.activity as HierarchyActivity?
            a?.newFolder()
        }
        root.findViewById<LinearLayout>(R.id.item_root).setOnClickListener {
            val a = this@NoFilesFragment.activity as HierarchyActivity?
            a?.hierarchyOperations?.goUp()
        }

        root.findViewById<TextView>(R.id.text).text = ".."
        root.findViewById<View>(R.id.changed_ago).visibility = View.GONE
        root.findViewById<View>(R.id.image).visibility = View.GONE
        root.findViewById<LinearLayout>(R.id.item_root)?.visibility = if (canGoUp) View.VISIBLE else View.GONE

        return root
    }

}