package com.somobu.gitdesktop.hierarchy

import android.content.Context
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.somobu.gitdesktop.R
import java.io.File


abstract class FileAdapter(context: Context) : ArrayAdapter<File>(context, R.layout.item_hierarchy) {

    private var files = emptyArray<File?>()
    private var selected = mutableListOf<File>()

    fun setItems(items: Array<File?>, selected: MutableList<File>) {
        this.selected = selected
        this.files = items

        files.sortWith { a, b ->
            when {
                (a == null && b == null) -> 0
                (a == null) -> -1
                (b == null) -> 1
                (a.isDirectory && b.isDirectory) -> a.compareTo(b)
                (a.isDirectory) -> -1
                (b.isDirectory) -> 1
                else -> a.compareTo(b)
            }
        }

        notifyDataSetChanged()
    }

    override fun getCount(): Int {
        return files.size
    }

    override fun getItem(position: Int): File? {
        return files[position]
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val file: File = getItem(position)!!

        val rootView = convertView ?: LayoutInflater.from(context)
            .inflate(R.layout.item_hierarchy, parent, false)

        rootView.setBackgroundResource(
            if (file in selected) com.somobu.essentials.R.color.orange_transparent
            else android.R.color.transparent
        )

        val suffix = if (file.isDirectory) "/" else ""
        val itemName = file.name + suffix
        val itemDate = "" + DateUtils.getRelativeTimeSpanString(
            file.lastModified(),
            System.currentTimeMillis(),
            DateUtils.MINUTE_IN_MILLIS,
            DateUtils.FORMAT_ABBREV_RELATIVE
        )

        rootView.findViewById<TextView>(R.id.text).text = itemName
        rootView.findViewById<TextView>(R.id.changed_ago).text = itemDate


        val img = rootView.findViewById<ImageView>(R.id.image)
        img.setImageResource(
            if (file.isDirectory) R.drawable.ic_folder
            else filetypeIcon(file.name)
        )

        val sel = rootView.findViewById<ImageView>(R.id.selection_mark)
        sel.setImageResource(
            if (file in selected) R.drawable.ic_done
            else android.R.color.transparent
        )

        sel.setOnClickListener {
            val wasSelected = file in selected
            if (wasSelected) selected.remove(file)
            else selected.add(file)
            notifyDataSetChanged()

            onSelectionChanged(file, !wasSelected)
        }


        return rootView
    }

    private fun filetypeIcon(name: String): Int {
        val dotIdx = name.lastIndexOf(".")

        var extension = ""
        if (dotIdx >= 0) extension = name.substring(dotIdx + 1)

        return when (extension) {
            "md" -> R.drawable.ic_markdown
            "txt", "doc", "docx", "odt" -> R.drawable.ic_file
            "png", "jpg", "jpeg", "gif", "svg" -> R.drawable.ic_image
            else -> R.drawable.ic_question
        }
    }

    abstract fun onSelectionChanged(file: File, isSelected: Boolean)
}
