package com.somobu.gitdesktop

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast


class ManualActivity : Activity() {

    companion object {

        fun start(context: Context, anchor: String?){
            val intent = Intent(context, ManualActivity::class.java)
            intent.putExtra("fragment", anchor)
            context.startActivity(intent)
        }

    }

    val HELP_ASSET = "file:///android_asset/users_manual.html"

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val view = WebView(this)
        view.settings.javaScriptEnabled = true
        setContentView(view)

        val fragment = intent.getStringExtra("fragment")
        view.webViewClient = object : WebViewClient() {

            override fun onPageFinished(view: WebView, url: String?) {
                super.onPageFinished(view, url)

                if (url == HELP_ASSET && fragment != null) {
                    view.evaluateJavascript("window.location.href = \"#$fragment\";", null)
                }
            }

            @Deprecated("Deprecated in Java")
            override fun shouldOverrideUrlLoading(view: WebView, url: String?): Boolean {
                if (url != null && (url.startsWith("http://") || url.startsWith("https://"))) {
                    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

                    try {
                        view.context.startActivity(intent)
                    } catch (e: Exception) {
                        Toast.makeText(view.context, "Unable to open link in browser!", Toast.LENGTH_SHORT).show();
                    }

                    return true
                } else {
                    return false
                }
            }
        }

        view.loadUrl(HELP_ASSET)
    }

}