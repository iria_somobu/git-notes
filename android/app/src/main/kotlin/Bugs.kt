package com.somobu.gitdesktop

import android.app.ListFragment
import android.app.PendingIntent
import android.os.Build
import info.guardianproject.netcipher.NetCipher
import org.eclipse.jgit.transport.HttpTransport
import org.eclipse.jgit.transport.TransportHttp
import org.eclipse.jgit.transport.http.HttpConnection
import org.eclipse.jgit.transport.http.HttpConnectionFactory
import org.eclipse.jgit.transport.http.JDKHttpConnection
import java.net.Proxy
import java.net.URL


object Bugs {

    /**
     * On Android < 5.0 you may face with errors related to SSLv3
     */
    fun patchJgit() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            println("Patching jgit's http transport")
            TransportHttp.setConnectionFactory(PatchedConnectionFactory())
            HttpTransport.setConnectionFactory(PatchedConnectionFactory())
        }
    }

    class PatchedConnectionFactory : HttpConnectionFactory {
        override fun create(url: URL): HttpConnection = create(url, null);
        override fun create(url: URL, proxy: Proxy?): HttpConnection = PatchedConnection(url, proxy)
    }

    class PatchedConnection(url: URL, proxy: Proxy?) : JDKHttpConnection(url, proxy) {

        init {
            try {
                val declaredField = JDKHttpConnection::class.java.getDeclaredField("wrappedUrlConnection")
                declaredField.isAccessible = true
                declaredField.set(this, NetCipher.getHttpsURLConnection(url))
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }
    }

    fun preventInfiniteListLoading(fragment: ListFragment) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            fragment.setListShown(true)
        }
    }
}