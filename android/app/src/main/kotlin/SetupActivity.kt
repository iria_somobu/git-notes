@file:Suppress("DEPRECATION")

package com.somobu.gitdesktop

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.widget.*
import com.somobu.gitdesktop.hierarchy.HierarchyActivity
import com.somobu.gitdesktop.sync.SyncHelper

class SetupActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        actionBar?.hide()
        setContentView(R.layout.activity_setup)

        val cbRemote = findViewById<CheckBox>(R.id.checkbox_remote)
        val cbCredentials = findViewById<CheckBox>(R.id.checkbox_credentials)

        cbRemote.setOnCheckedChangeListener { _, isChecked ->
            findViewById<LinearLayout>(R.id.layout_remote).visibility =
                if (isChecked) LinearLayout.VISIBLE else LinearLayout.GONE
        }

        cbCredentials.setOnCheckedChangeListener { _, isChecked ->
            findViewById<LinearLayout>(R.id.layout_credentials).visibility =
                if (isChecked) LinearLayout.VISIBLE else LinearLayout.GONE
        }

        cbRemote.isChecked = false
        cbCredentials.isChecked = false

        findViewById<Button>(R.id.button).setOnClickListener {

            var git_user = "root"
            var git_mail = "root@localhost"

            if (cbCredentials.isChecked) {
                git_user = findViewById<EditText>(R.id.git_user).text.toString().trim()
                git_mail = findViewById<EditText>(R.id.git_email).text.toString().trim()
            }

            if (!cbRemote.isChecked) {
                GitOps.updateLastSyncTime(this)
            }

            PreferenceManager.getDefaultSharedPreferences(this)
                .edit()
                .putString("GIT_USER", git_user)
                .putString("GIT_MAIL", git_mail)
                .putBoolean("REPO_REMOTE", cbRemote.isChecked)
                .putString("REPO_URL", findViewById<EditText>(R.id.repo).text.toString().trim())
                .putString("REPO_USER", findViewById<EditText>(R.id.username).text.toString().trim())
                .putString("REPO_PWD", findViewById<EditText>(R.id.password).text.toString().trim())
                .putBoolean("INIT_DONE", true)
                .apply()

            startActivity(Intent(this, HierarchyActivity::class.java))
            SyncHelper.scheduleSync(this)
            GitOpsService.sync(this)
            finish()
        }

        findViewById<ImageButton>(R.id.users_manual).setOnClickListener {
            ManualActivity.start(this, "Setup_screen")
        }

        findViewById<TextView>(R.id.git_help).setOnClickListener {
            ManualActivity.start(this, "What_is_git_name_and_email?")
        }

        findViewById<TextView>(R.id.repo_help).setOnClickListener {
            ManualActivity.start(this, "How_do_I_create_an_account?")
        }
    }

}