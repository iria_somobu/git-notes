package com.somobu.gitdesktop

import android.content.Context
import android.os.Handler
import java.io.IOException


class ShellHelper {

    companion object StaticMethods {

        @Throws(IOException::class)
        fun exec(bash: String): Process {
            return Runtime.getRuntime().exec(arrayOf("/system/bin/sh", "-e", "-c", bash))
        }

        fun execAsync(bash: String, ctx: Context, after: Runnable) {
            val thread = object : Thread() {
                override fun run() {
                    try {
                        val p: Process = exec(bash)
                        val rz = p.waitFor()
                        // TODO: check result code maybe?
                    } finally {
                        Handler(ctx.mainLooper).post(after)
                    }
                }
            }
            thread.start()
        }

    }

}