package com.somobu.gitdesktop.sync

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.somobu.gitdesktop.GitOpsService

class SyncReceiver : BroadcastReceiver() {

    override fun onReceive(ctx: Context?, p1: Intent?) {
        GitOpsService.sync(ctx!!)
        SyncHelper.scheduleSync(ctx)
    }
}