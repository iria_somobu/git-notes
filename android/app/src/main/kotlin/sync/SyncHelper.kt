package com.somobu.gitdesktop.sync

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import java.util.*
import kotlin.math.floor


object SyncHelper {

    private const val SYNC_INTERVAL_MINUTES = 20;

    fun scheduleSync(context: Context) {
        val notifyTime = Calendar.getInstance()
        var minutes = notifyTime.get(Calendar.MINUTE)
        minutes = (floor(minutes / SYNC_INTERVAL_MINUTES.toFloat()) * SYNC_INTERVAL_MINUTES).toInt()

        notifyTime.set(Calendar.MINUTE, minutes)
        notifyTime.add(Calendar.MINUTE, SYNC_INTERVAL_MINUTES)
        notifyTime.set(Calendar.SECOND, 0)

        val i = Intent(context, SyncReceiver::class.java)
        val receiver = PendingIntent.getBroadcast(context, 0, i, Bugs.piImmutable())

        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmManager.cancel(receiver)
        alarmManager.set(AlarmManager.RTC_WAKEUP, notifyTime.timeInMillis, receiver)
    }

}