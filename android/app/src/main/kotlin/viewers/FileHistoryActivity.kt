package com.somobu.gitdesktop.viewers

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import android.widget.ListView
import android.widget.TextView
import com.somobu.gitdesktop.GitOps
import com.somobu.gitdesktop.Levenshtein
import com.somobu.gitdesktop.R
import io.noties.markwon.Markwon
import io.noties.markwon.ext.strikethrough.StrikethroughPlugin
import io.noties.markwon.ext.tasklist.TaskListPlugin
import org.eclipse.jgit.diff.DiffEntry
import org.eclipse.jgit.diff.DiffFormatter
import org.eclipse.jgit.lib.Constants
import org.eclipse.jgit.lib.ObjectId
import org.eclipse.jgit.lib.ObjectReader
import org.eclipse.jgit.revwalk.RevCommit
import org.eclipse.jgit.treewalk.CanonicalTreeParser
import java.io.ByteArrayOutputStream
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class FileHistoryActivity : Activity() {

    companion object {

        fun getIntent(ctx: Context, file: File): Intent {
            val intent = Intent(ctx, FileHistoryActivity::class.java)
            intent.putExtra("file", file.canonicalPath)
            return intent
        }

    }

    lateinit var adapter: Adapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_del_hist)

        adapter = Adapter(this)

        findViewById<LinearLayout>(R.id.contentless).visibility = View.VISIBLE

        val contentful = findViewById<ListView>(R.id.contentful)
        contentful.visibility = View.GONE
        contentful.adapter = adapter

        findViewById<TextView>(R.id.text).text = "Loading data"

        load()
    }

    private fun getFile(): String? {
        val file = intent?.getStringExtra("file")
        if (file == null) {
            return null
        } else {
            return File(file).canonicalPath
        }
    }

    fun load() {
        val filePath = getFile()!!.replace(GitO.getRepoDir(this).canonicalPath + "/", "")

        val handler = Handler()

        var suppressMsgs = false

        val work = object : GitOps.RepoWork() {

            @SuppressLint("SimpleDateFormat")
            fun sameDate(date1: Date, date2: Date): Boolean {
                val fmt = SimpleDateFormat("yyyyMMdd")
                return fmt.format(date1).equals(fmt.format(date2))
            }

            override fun run() {

                thread.onStatusMessage("Listing commits")
                val reader: ObjectReader = thread.git.repository.newObjectReader()

                val oldTreeIter = CanonicalTreeParser()
                val newTreeIter = CanonicalTreeParser()

                val head: ObjectId? = thread.git.repository.resolve(Constants.HEAD)
                val commits = thread.git.log().add(head)
                    .addPath(filePath)
                    .call()

                thread.onStatusMessage("Collecting changes")

                val fmt = SimpleDateFormat.getDateInstance()
                val list = arrayListOf<ParsedChanges>()
                var prevCommit: RevCommit? = null
                for (commit in commits) {

                    if (prevCommit != null) {
                        val prevDate = Date(prevCommit.commitTime * 1000L)
                        val currDate = Date(commit.commitTime * 1000L)
                        if (sameDate(prevDate, currDate)) continue

                        oldTreeIter.reset(reader, commit.tree)
                        newTreeIter.reset(reader, prevCommit.tree)

                        val entries =
                            thread.git.diff().setOldTree(oldTreeIter).setNewTree(newTreeIter).call()
                        for (entry: DiffEntry in entries) {
                            if (filePath == entry.newPath || filePath == entry.oldPath) {

                                val out = ByteArrayOutputStream()
                                val formatter = DiffFormatter(out)

                                formatter.setRepository(thread.git.repository)
                                formatter.format(entry)

                                formatter.flush()
                                list.add(
                                    ParsedChanges(
                                        out.toString(),
                                        prevCommit.id.abbreviate(5).toString(),
                                        fmt.format(Date(prevCommit.commitTime * 1000L))
                                    )
                                )

                                formatter.release()
                                out.close()
                            }
                        }
                    }

                    prevCommit = commit

                }

                thread.onStatusMessage("Parsing changes")

                var result = "";

                for (ch in list) {
                    if (ch.deleted.isNotEmpty()) {
                        result += "# " + ch.commitName
                        result += "\n" + ch.deleted.joinToString("\n") + "\n\n"
                    }
                }

                val properList = list.filter { it.deleted.isNotEmpty() }
                suppressMsgs = true
                handler.post { onChangesListReady(properList) }
            }

        }

        val thread = object : GitOps.RepoWorkerThread(this@FileHistoryActivity, work) {

            override fun onStatusMessage(message: String) {
                if (!suppressMsgs) {
                    handler.post { findViewById<TextView>(R.id.text).text = "Loading data\n\t$message" }
                }
            }

            override fun onCriticalError(error: GitOps.SyncFail) {
                if (!suppressMsgs) {
                    handler.post { findViewById<TextView>(R.id.text).text = "Error: " + error.name }
                }
            }

        }
        thread.start()

        handler.post {
            findViewById<LinearLayout>(R.id.contentless).visibility = View.VISIBLE
            findViewById<ListView>(R.id.contentful).visibility = View.GONE
        }
    }

    fun onChangesListReady(properList: List<ParsedChanges>) {
        if (properList.isNotEmpty()) {
            findViewById<LinearLayout>(R.id.contentless).visibility = View.GONE
            findViewById<ListView>(R.id.contentful).visibility = View.VISIBLE
            adapter.clear()
            adapter.addAll(properList)
        } else {
            findViewById<LinearLayout>(R.id.contentless).visibility = View.VISIBLE
            findViewById<ListView>(R.id.contentful).visibility = View.GONE
            findViewById<TextView>(R.id.text).text = getString(R.string.del_hist_intro)
        }
    }

    class ParsedChanges(source: String, val commitHash: String, val commitName: String) {

        val added: ArrayList<String> = arrayListOf()
        val deleted: ArrayList<String> = arrayListOf()
        val changed: ArrayList<String> = arrayListOf()

        init {

            for (line in source.lines()) {
                if (line.startsWith(" ")) continue
                if (line.startsWith("---")) continue
                if (line.startsWith("+++")) continue
                if (line.startsWith("@@")) continue

                if (line.startsWith("+")) {
                    val a = line.substring(1)
                    if (a.trim().isNotEmpty()) added.add(a)
                } else if (line.startsWith("-")) {
                    val a = line.substring(1)
                    if (a.trim().isNotEmpty()) deleted.add(a)
                }
            }

            for (deletedLine in deleted) {
                var minLevi = 1f
                var addedLine = ""
                for (add in added) {
                    val dist = Levenshtein.normalizedDistance(deletedLine, add)
                    if (dist < minLevi) {
                        minLevi = dist
                        addedLine = add
                    }
                }

                if (addedLine.isNotEmpty() && minLevi < 0.4) {
                    changed.add(deletedLine)
                    added.remove(addedLine)
                }
            }

            deleted.removeAll(changed.toSet())
        }

        override fun toString(): String {
            val a = if (added.isNotEmpty()) "+ " + added.joinToString("\n+ ") else ""
            val b = if (deleted.isNotEmpty()) "\n- " + deleted.joinToString("\n- ") else ""
            val c = if (changed.isNotEmpty()) "\n± " + changed.joinToString("\n± ") else ""
            return a + b + c
        }
    }

    class Adapter(context: Context) : ArrayAdapter<ParsedChanges>(context, R.layout.item_diff) {

        private val inflater: LayoutInflater = LayoutInflater.from(context)

        private val markwon = Markwon.builder(context)
            .usePlugin(TaskListPlugin.create(context))
            .usePlugin(StrikethroughPlugin.create())
            .build()

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            var convertView = convertView
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.item_diff, parent, false)
            }

            val item = getItem(position)!!
            convertView!!.findViewById<TextView>(R.id.item_heading).text = item.commitName
            markwon.setMarkdown(convertView.findViewById(R.id.item_text), item.deleted.joinToString("\n"))

            return convertView
        }

    }
}