package com.somobu.gitdesktop

import kotlin.math.max
import kotlin.math.min

/**
 * From Apache Commons Text
 */
object Levenshtein {

    fun normalizedDistance(left: CharSequence, right: CharSequence): Float {
        val mlen = max(left.length, right.length)

        if (mlen == 0) return 0f
        else return distance(left, right) / mlen.toFloat()
    }

    /**
     * Finds the Levenshtein distance between two Strings.
     *
     * A higher score indicates a greater distance.
     *
     * The previous implementation of the Levenshtein distance algorithm
     * was from [https://web.archive.org/web/20120526085419/http://www.merriampark.com/ldjava.htm](https://web.archive.org/web/20120526085419/http://www.merriampark.com/ldjava.htm)
     *
     * This implementation only need one single-dimensional arrays of length s.length() + 1
     *
     * <pre>
     * unlimitedCompare(null, *)             = IllegalArgumentException
     * unlimitedCompare(*, null)             = IllegalArgumentException
     * unlimitedCompare("","")               = 0
     * unlimitedCompare("","a")              = 1
     * unlimitedCompare("aaapppp", "")       = 7
     * unlimitedCompare("frog", "fog")       = 1
     * unlimitedCompare("fly", "ant")        = 3
     * unlimitedCompare("elephant", "hippo") = 7
     * unlimitedCompare("hippo", "elephant") = 7
     * unlimitedCompare("hippo", "zzzzzzzz") = 8
     * unlimitedCompare("hello", "hallo")    = 1
     * </pre>
     *
     * @param left the first CharSequence, must not be null
     * @param right the second CharSequence, must not be null
     * @return result distance, or -1
     * @throws IllegalArgumentException if either CharSequence input is `null`
     */
    fun distance(left: CharSequence, right: CharSequence): Int {
        var left: CharSequence = left
        var right: CharSequence = right

        /*
         * This implementation use two variable to record the previous cost counts,
         * this implementation use less memory than previous impl.
         */
        var n = left.length // length of left
        var m = right.length // length of right

        if (n == 0) {
            return m
        }

        if (m == 0) {
            return n
        }

        if (n > m) {
            // swap the input strings to consume less memory
            val tmp: CharSequence = left
            left = right
            right = tmp
            n = m
            m = right.length
        }

        val p = IntArray(n + 1)

        // indexes into strings left and right
        var i: Int // iterates through left
        var j: Int // iterates through right

        var upperLeft: Int
        var upper: Int
        var rightJ: Char // jth character of right
        var cost: Int // cost

        i = 0
        while (i <= n) {
            p[i] = i
            i++
        }
        j = 1

        while (j <= m) {
            upperLeft = p[0]
            rightJ = right[j - 1]
            p[0] = j
            i = 1
            while (i <= n) {
                upper = p[i]
                cost = if (left[i - 1] == rightJ) 0 else 1
                // minimum of cell to the left+1, to the top+1, diagonally left and up +cost
                p[i] = min(min(p[i - 1] + 1, p[i] + 1), upperLeft + cost)
                upperLeft = upper
                i++
            }
            j++
        }

        return p[n]
    }


}