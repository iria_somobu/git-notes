package com.somobu.gitdesktop

import GitO
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Handler
import android.preference.PreferenceManager
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.api.errors.*
import org.eclipse.jgit.lib.PersonIdent
import org.eclipse.jgit.storage.file.FileRepositoryBuilder
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class GitOps {

    companion object {
        const val ACTION_COMMIT = "com.somobu.gitdesktop.ACTION_COMMIT"
        const val ACTION_SYNC = "com.somobu.gitdesktop.ACTION_SYNC"

        const val BROADCAST_STATUS = "com.somobu.gitdesktop.GIT_STATUS"
        const val BROADCAST_ERROR = "com.somobu.gitdesktop.GIT_ERROR"
        const val BROADCAST_PULLED = "com.somobu.gitdesktop.GIT_PULLED"
        const val BROADCAST_FINISHED = "com.somobu.gitdesktop.GIT_FINISHED"
        const val BROADCAST_CONFLICT = "com.somobu.gitdesktop.GIT_CONFLICT"

        /**
         * Just a helper method, have no idea where to put it
         */
        fun forceDeleteIndexLock(ctx: Context) {
            val gitDir = GitO.getGitDir(ctx)

            val lockFile = File("${gitDir.canonicalPath}/index.lock")
            if (lockFile.exists()) lockFile.delete()
        }

        fun updateLastSyncTime(ctx: Context) {
            Handler(ctx.mainLooper).post {
                val date = SimpleDateFormat.getDateTimeInstance().format(Date())
                PreferenceManager.getDefaultSharedPreferences(ctx)
                    .edit()
                    .putString("SYNC_TIME_STAT", date)
                    .commit()
            }
        }
    }

    enum class SyncFail {
        LOCKED_INDEX,
        UNAUTHORIZED,
        NETWORK_ERROR,
        INVALID_REMOTE,
        TRANSPORT_ERROR
    }

    open class RepoWorkerThread(val ctx: Context, private val work: RepoWork) : Thread() {

        val handler = Handler(ctx.mainLooper)

        lateinit var git: Git
        lateinit var ident: PersonIdent
        var auth: UsernamePasswordCredentialsProvider? = null

        /**
         * Just for UI
         */
        var hasError = false

        companion object {
            private val syncGuard = Object()
        }

        private fun e(e: Exception) {
            hasError = true
            e.printStackTrace()
        }

        override fun run() {
            val time = System.currentTimeMillis()
            synchronized(syncGuard) {
                println("Awaited guard: ${System.currentTimeMillis() - time} ms")
                Bugs.patchJgit()

                val prefs = PreferenceManager.getDefaultSharedPreferences(ctx)
                val git_user = prefs.getString("GIT_USER", null)
                val git_mail = prefs.getString("GIT_MAIL", null)
                val is_remote = prefs.getBoolean("REPO_REMOTE", false)
                val repo_url = prefs.getString("REPO_URL", null)
                val repo_usr = prefs.getString("REPO_USER", null)
                val repo_pwd = prefs.getString("REPO_PWD", null)

                try {
                    onStatusMessage("Setting up repo")

                    val parentDir = GitO.getRepoDir(ctx)
                    val gitDir = GitO.getGitDir(ctx)

                    val lockFile = File("$gitDir/index.lock")
                    if (lockFile.exists()) {
                        onCriticalError(SyncFail.LOCKED_INDEX)
                        throw java.lang.RuntimeException("Git index locked")
                    }

                    ident = PersonIdent(git_user, git_mail)
                    auth = if (is_remote) UsernamePasswordCredentialsProvider(repo_usr, repo_pwd)
                    else null

                    if (gitDir.exists() || !is_remote) {
                        val repo = FileRepositoryBuilder()
                            .setGitDir(gitDir)
                            .findGitDir()
                            .build()

                        try {
                            repo.create()
                        } catch (e: IllegalStateException) {
                            // We're ignoring exception this kind of exceptions
                            if (e.message?.startsWith("Repository already exists") != true) {
                                throw e
                            }
                        }

                        git = Git(repo)
                    } else {
                        git = Git.cloneRepository()
                            .setURI(repo_url)
                            .setDirectory(parentDir)
                            .setCredentialsProvider(auth)
                            .call()
                    }

                    try {
                        work.thread = this
                        work.run()
                    } catch (e: CheckoutConflictException) {
                        onStatusMessage("Checkout conflict, doing my best")
                        // TODO: What should I do here?
                    } catch (e: InvalidRemoteException) {
                        e(e)
                        onCriticalError(SyncFail.INVALID_REMOTE)
                    } catch (e: TransportException) {
                        e(e)
                        if (e.message.toString().toLowerCase().contains("not authorized")) {
                            onCriticalError(SyncFail.UNAUTHORIZED)
                        } else {
                            onCriticalError(SyncFail.TRANSPORT_ERROR)
                        }
                    } catch (e: Exception) {
                        e(e)
                        onStatusMessage(e.message.toString())
                    }

                    if (!hasError) onStatusMessage("Finalizing")
                    git.close()
                    if (!hasError) onStatusMessage("Sync done")

                } catch (e: WrongRepositoryStateException) {
                    if (e.message!!.contains("MERGING")) {
                        onStatusMessage("Repo merging error")
                    } else {
                        e(e)
                    }
                } catch (e: InvalidRemoteException) {
                    onCriticalError(SyncFail.INVALID_REMOTE)
                } catch (e: TransportException) {
                    e(e)
                    onStatusMessage("Transport error")

                    if ((e.message + "").contains("cannot open git-receive-pack")) {
                        onCriticalError(SyncFail.NETWORK_ERROR)
                    } else {
                        onCriticalError(SyncFail.TRANSPORT_ERROR)
                    }
                } catch (e: Exception) {
                    e(e)
                    onStatusMessage(e.message.toString())
                } finally {
                    val intent = Intent()
                    intent.action = BROADCAST_FINISHED
                    ctx.sendBroadcast(intent)
                }
            }
        }

        fun pull() {
            if (auth != null) {
                onStatusMessage("Pulling")
                git.pull().setCredentialsProvider(auth).call()
            }

            handler.post {
                val intent = Intent()
                intent.action = BROADCAST_PULLED
                ctx.sendBroadcast(intent)
            }
        }

        fun commit(message: String) {
            onStatusMessage("Commit")

            // We have to call add() twice to make jgit add sub-folders to stage
            // see https://stackoverflow.com/a/40622293

            try {
                git.add().addFilepattern(".").call()
            } catch (e: JGitInternalException) {
                // TODO: what should I do here?
                // We might face this exception when there are no files in repo
                e(e)
            }

            git.add().setUpdate(true).addFilepattern(".").call();
            git.commit().setMessage(message).setAuthor(ident).call()
        }

        fun autoCommit() {
            val status = git.status().call()
            if (!status.isClean) {
                val date = SimpleDateFormat("yyyy.MM.dd HH:mm")
                    .format(Date(System.currentTimeMillis()))
                val device = Build.MODEL
                commit("$date $device")
            }
        }

        open fun onStatusMessage(message: String) {
            println("GitOps status: $message")

            handler.post {
                val intent = Intent()
                intent.action = BROADCAST_STATUS
                intent.putExtra("message", message)
                ctx.sendBroadcast(intent)
            }
        }

        open fun onCriticalError(error: SyncFail) {
            println("GitOps error: $error")

            hasError = true

            handler.post {
                val intent = Intent()
                intent.action = BROADCAST_ERROR
                intent.putExtra("error", error)
                ctx.sendBroadcast(intent)
            }
        }
    }

    abstract class RepoWork {
        lateinit var thread: RepoWorkerThread
        abstract fun run()
    }

    class Commit(private val message: String) : RepoWork() {
        override fun run() = thread.commit(message)
    }

    class Sync : RepoWork() {
        override fun run() {
            thread.pull()
            thread.autoCommit()

            if (thread.auth != null) {
                thread.onStatusMessage("Push")
                thread.git.push().setCredentialsProvider(thread.auth).call()
            }
        }
    }
}