package com.somobu.gitdesktop

import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Handler
import android.os.IBinder
import android.preference.PreferenceManager
import java.text.SimpleDateFormat
import java.util.*

/**
 *
 */
class GitOpsService : Service() {

    companion object {

        fun commit(ctx: Context, message: String) {
            val intent = Intent(ctx, GitOpsService::class.java)
            intent.action = GitOps.ACTION_COMMIT
            intent.putExtra("message", message)

            try {
                ctx.startService(intent)
            } catch (ignored: java.lang.Exception) {

            }
        }

        fun sync(ctx: Context) {
            val intent = Intent(ctx, GitOpsService::class.java)
            intent.action = GitOps.ACTION_SYNC

            try {
                ctx.startService(intent)
            } catch (ignored: java.lang.Exception) {

            }
        }
    }

    override fun onBind(p0: Intent?): IBinder? = null


    val startIdGuard = Object()
    var latestStartId = -1;

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {

        synchronized(startIdGuard) {
            latestStartId = startId
        }

        var work: GitOps.RepoWork? = null
        when (intent.action) {
            GitOps.ACTION_COMMIT -> work = GitOps.Commit(intent.getStringExtra("message")!!)
            GitOps.ACTION_SYNC -> work = GitOps.Sync()
        }

        val thread = object : GitOps.RepoWorkerThread(this@GitOpsService, work!!) {

            override fun run() {
                try {
                    super.run()
                } catch (_: Exception) {

                }

                GitOps.updateLastSyncTime(this@GitOpsService)

                Handler(this@GitOpsService.mainLooper).post {
                    val intent = Intent()
                    intent.action = GitOps.BROADCAST_FINISHED
                    this@GitOpsService.sendBroadcast(intent)
                }


                // Let this thread sleep for a second to allow thread's handler make its work
                // If you remove this, all thread data (including handler) will be destroyed ASAP
                sleep(1000)

                synchronized(startIdGuard) {
                    if (latestStartId == startId) {
                        val myPid = android.os.Process.myPid()
                        android.os.Process.killProcess(myPid)
                    }
                }
            }

        }
        thread.isDaemon = true
        thread.start()

        return START_NOT_STICKY
    }
}