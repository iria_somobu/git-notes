package com.somobu.viewtxt

import android.app.Fragment
import com.somobu.essentials.EditorActivity
import com.somobu.essentials.TextEditorFragment
import java.io.File

@Suppress("DEPRECATION")
class TextEditorActivity : EditorActivity() {

    override fun getTextEditorFragment(file: File): TextEditorFragment {
        return MyTextEditorFragment.instance(file)
    }

    override fun getViewerFragment(file: File): Fragment {
        return TextViewerFragment.instance(file)
    }


}