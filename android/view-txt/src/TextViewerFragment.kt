@file:Suppress("DEPRECATION")

package com.somobu.viewtxt

import android.app.Fragment
import android.content.Context
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.ScrollView
import android.widget.TextView
import com.somobu.essentials.CustomizedActivity
import io.noties.markwon.Markwon
import io.noties.markwon.ext.strikethrough.StrikethroughPlugin
import io.noties.markwon.ext.tasklist.TaskListPlugin
import java.io.File

class TextViewerFragment : Fragment() {

    companion object StaticMethods {

        fun instance(file: File): TextViewerFragment {
            val f = TextViewerFragment()

            val args = Bundle()
            args.putString("FILE", file.absolutePath)
            f.arguments = args

            return f
        }
    }

    private var markwon: Markwon? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    private fun getFile(): File {
        return File(arguments.getString("FILE")!!)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_md_view, container, false)
        val contentful = view.findViewById<ScrollView>(R.id.contentful)
        val textView = view.findViewById<TextView>(R.id.text)

        val markwon = Markwon.builder(view.context)
            .usePlugin(TaskListPlugin.create(view.context))
            .usePlugin(StrikethroughPlugin.create())
            .build()

        if (getFile().name.endsWith(".md")) {
            markwon.setMarkdown(textView, getFile().readText())
        } else {
            textView.text = getFile().readText()
        }

        textView.movementMethod = LinkMovementMethod.getInstance()
        
        val empty = textView.text.trim().isEmpty()
        contentful.visibility = if (empty) View.GONE else View.VISIBLE
        view.findViewById<LinearLayout>(R.id.contentless).visibility =
            if (empty) View.VISIBLE else View.GONE


        view.findViewById<ImageButton>(R.id.btn_switch_to_edit)
            .setOnClickListener({
                run {
                    if (activity is TextEditorActivity) {
                        val activity = activity as TextEditorActivity
                        activity.toggleEditor()
                    }
                }
            })

        contentful.viewTreeObserver.addOnScrollChangedListener(object : ViewTreeObserver.OnScrollChangedListener {

            var prevScroll: Int = 0

            override fun onScrollChanged() {
                val currentScroll = contentful.scrollY
                val maActivity = this@TextViewerFragment.activity as CustomizedActivity?
                maActivity?.handleScrollY((prevScroll - currentScroll).toFloat())
                prevScroll = currentScroll
            }

        })

        return view
    }

}