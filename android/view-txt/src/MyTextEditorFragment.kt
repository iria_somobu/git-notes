@file:Suppress("DEPRECATION")

package com.somobu.viewtxt

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.EditText
import android.widget.ScrollView
import com.somobu.essentials.CustomizedActivity
import com.somobu.essentials.ITextEditHost
import com.somobu.essentials.TextEditorFragment
import io.noties.markwon.Markwon
import io.noties.markwon.editor.MarkwonEditor
import io.noties.markwon.editor.MarkwonEditorTextWatcher
import io.noties.markwon.ext.tasklist.TaskListPlugin
import java.io.File
import java.util.concurrent.Executors

class MyTextEditorFragment : TextEditorFragment() {

    companion object StaticMethods {

        fun instance(file: File): MyTextEditorFragment {
            val f = MyTextEditorFragment()

            val args = Bundle()
            args.putString("FILE", file.absolutePath)
            f.arguments = args

            return f
        }
    }

    private var textChanged = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_md_edit, container, false)
        val editText = view.findViewById<EditText>(R.id.text)

        val markwon = Markwon.builder(view.context)
            .usePlugin(TaskListPlugin.create(view.context))
            .build()

        val editor = MarkwonEditor.create(markwon)

        if (getFile().name.endsWith(".md")) {
            editText.addTextChangedListener(
                MarkwonEditorTextWatcher.withPreRender(
                    editor,
                    Executors.newCachedThreadPool(),
                    editText
                )
            )
        }

        editText.setText(getFile().readText())

        // Set own text watcher AFTER setText() to prevent trigger from MarkWon changing text appearance
        editText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun afterTextChanged(p0: Editable?) {
                textChanged = true
                (activity as ITextEditHost).onTextChanged()
            }

        })

        val scroll = view.findViewById<ScrollView>(R.id.scroll)
        scroll.viewTreeObserver.addOnScrollChangedListener(object : ViewTreeObserver.OnScrollChangedListener {

            var prevScroll: Int = 0

            override fun onScrollChanged() {
                val currentScroll = scroll.scrollY
                val maActivity = this@MyTextEditorFragment.activity as CustomizedActivity?
                maActivity?.handleScrollY((prevScroll - currentScroll).toFloat())
                prevScroll = currentScroll
            }

        })

        return view
    }

    private fun getFile(): File {
        return File(arguments.getString("FILE")!!)
    }

    override fun isTextChanged(): Boolean {
        return textChanged
    }

    override fun markSaved() {
        textChanged = false
    }

    override fun getText(): String {
        return view?.findViewById<EditText>(R.id.text)?.text.toString()
    }
}