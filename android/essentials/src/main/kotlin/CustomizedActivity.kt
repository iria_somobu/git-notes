package com.somobu.essentials

import android.app.Activity
import android.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast

@Suppress("DEPRECATION")
abstract class CustomizedActivity : Activity() {

    companion object {
        const val GROUP_NEVER = "mode_never"
        const val GROUP_ALWAYS = "mode_always"
        const val GROUP_DEFAULT = "mode_default"
        const val GROUP_SELECTION = "mode_selection"
    }

    abstract class Btn {
        abstract fun getGroup(): String
        abstract fun getIconRes(): Int
        abstract fun getLabelRes(): Int
        abstract fun click(): ((View) -> Unit)
    }

    private lateinit var header: View
    private lateinit var footer: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        header = findViewById(R.id.header_block)
        footer = findViewById(R.id.footer_block)

        setHomeButton(false, null)
        refreshBottomButtons()
        switchMode(false)
    }

    abstract fun getBottomButtons(): Array<Btn>

    fun refreshBottomButtons() {
        val group_default = findViewById<LinearLayout>(R.id.group_default)
        val group_selection = findViewById<LinearLayout>(R.id.group_selection)
        val group_always = findViewById<LinearLayout>(R.id.group_always)

        val groups = arrayOf(group_default, group_selection, group_always)
        for (group in groups) group.removeAllViews()

        val inflater = LayoutInflater.from(this)!!
        for (button in getBottomButtons()) {
            val group = when (button.getGroup()) {
                GROUP_DEFAULT -> group_default
                GROUP_SELECTION -> group_selection
                GROUP_NEVER -> null
                else -> group_always
            }

            if (group == null) continue

            val btnView = inflater.inflate(R.layout.item_button, group, false)
            (btnView as ImageButton).setImageResource(button.getIconRes())
            btnView.setOnClickListener(button.click())

            btnView.setOnLongClickListener {
                Toast.makeText(this, button.getLabelRes(), Toast.LENGTH_SHORT).show()
                true
            }

            group.addView(btnView)
        }
    }

    fun switchMode(isSelection: Boolean) {
        val visible = if (isSelection) View.VISIBLE else View.GONE
        val gone = if (isSelection) View.GONE else View.VISIBLE

        findViewById<LinearLayout>(R.id.group_selection).visibility = visible
        findViewById<LinearLayout>(R.id.group_default).visibility = gone

        findViewById<View>(R.id.sel_container).visibility = visible
        findViewById<View>(R.id.sel_divider).visibility = visible
    }

    fun setSelectionModeLabel(text: String) {
        findViewById<TextView>(R.id.sel_label).text = text
    }

    fun setHeaderTitle(title: String?) {
        findViewById<TextView>(R.id.header_title).text = title
    }

    fun setHeaderSubtitle(title: String?) {
        val view = findViewById<TextView>(R.id.header_subtitle)
        view.text = title
        view.visibility = if (title == null) View.GONE else View.VISIBLE
    }

    fun setHeaderColor(color: Int) {
        findViewById<LinearLayout>(R.id.header).setBackgroundColor(color)
    }

    fun setHomeButton(enabled: Boolean, listener: ((View) -> Unit)?, resource: Int = 0) {
        val v = findViewById<ImageButton>(R.id.btn_home_up)
        if (resource != 0) v.setImageResource(resource)
        v.visibility = if (enabled) View.VISIBLE else View.GONE
        v.setOnClickListener(listener)
    }

    fun handleScrollY(delta: Float) {
        header.translationY += delta
        if (header.translationY < -header.height) header.translationY = -header.height.toFloat()
        if (header.translationY > 0) header.translationY = 0f

        footer.translationY -= delta
        if (footer.translationY > footer.height) footer.translationY = footer.height.toFloat()
        if (footer.translationY < 0) footer.translationY = 0f
    }

    fun getCurrentFragment(): Fragment? {
        return fragmentManager.findFragmentById(R.id.container);
    }

    fun setFragment(fragment: Fragment) {
        fragmentManager.beginTransaction()
            .replace(R.id.container, fragment)
            .commitAllowingStateLoss()
    }
}