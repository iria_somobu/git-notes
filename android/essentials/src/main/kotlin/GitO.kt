import android.content.Context
import java.io.File

class GitO {

    companion object {

        fun getRepoDir(ctx: Context): File {
            return File(ctx.filesDir!!.canonicalPath)
//            return ctx.getExternalFilesDir(null)!!
        }

        fun getGitDir(ctx: Context): File {
            return File(getRepoDir(ctx), ".git")
        }

    }

}