@file:Suppress("DEPRECATION")

package com.somobu.essentials

import android.app.Fragment

abstract class TextEditorFragment : Fragment() {

    abstract fun isTextChanged(): Boolean

    abstract fun markSaved()

    abstract fun getText(): String

}