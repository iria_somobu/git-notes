package com.somobu.essentials

interface ITextEditHost {

    fun onTextChanged()

}