package com.somobu.essentials

import GitO
import android.app.AlertDialog
import android.content.Context
import android.widget.ArrayAdapter
import android.widget.ListView
import java.io.File

class FileChooser {

    companion object {

        fun pickFile(
            ctx: Context,
            title: String,
            initialDir: File,
            success: (File) -> Unit,
            cancel: () -> Unit
        ) {
            dialog(ctx, title, initialDir, false, success, cancel)
        }

        fun pickFolder(
            ctx: Context,
            title: String,
            initialDir: File,
            success: (File) -> Unit,
            cancel: () -> Unit
        ) {
            dialog(ctx, title, initialDir, true, success, cancel)
        }

        private fun dialog(
            ctx: Context,
            title: String,
            initialDir: File,
            dirsOnly: Boolean,
            success: (File) -> Unit,
            cancel: () -> Unit
        ) {
            val maxDir = GitO.getRepoDir(ctx)
            var currentDir = initialDir

            val list = ListView(ctx)

            // Prepare dialog itself
            var builder = AlertDialog.Builder(ctx)
                .setTitle(title)
                .setView(list)
                .setCancelable(false)
                .setNegativeButton("Cancel", { _, _ ->
                    run {
                        cancel()
                    }
                })

            if (dirsOnly) {
                builder = builder.setPositiveButton("Choose") { _, _ ->
                    run {
                        success(currentDir)
                    }
                }
            }

            val dialog = builder.show()


            // Setup files list
            val adapter = ArrayAdapter<String>(ctx, android.R.layout.simple_list_item_1)
            list.adapter = adapter

            if (canGoUp(currentDir, maxDir)) adapter.add("..")
            adapter.addAll(if (dirsOnly) listFoldersNames(currentDir) else listFilesNames(currentDir))
            adapter.notifyDataSetChanged()
            dialog.setTitle(title + title(currentDir, maxDir))

            list.setOnItemClickListener { _, _, position, _ ->
                run {
                    val selectedFile = File(currentDir, adapter.getItem(position)!!)

                    if (!selectedFile.isDirectory) {
                        if (dirsOnly) throw RuntimeException("We got file in 'dirs only' mode somehow")

                        success(selectedFile)
                        dialog.dismiss()
                        return@run
                    }

                    currentDir = selectedFile

                    adapter.clear()

                    if (canGoUp(currentDir, maxDir)) adapter.add("..")
                    adapter.addAll(
                        if (dirsOnly) listFoldersNames(currentDir)
                        else listFilesNames(currentDir)
                    )
                    adapter.notifyDataSetChanged()

                    dialog.setTitle(title + title(currentDir, maxDir))
                }
            }
        }

        private fun title(current: File, limit: File): String {
            return current.canonicalPath.substring(limit.canonicalPath.length) + "/"
        }

        private fun canGoUp(current: File, limit: File): Boolean {
            val shorter = limit.canonicalPath
            val longer = current.canonicalPath

            if (!longer.startsWith(shorter)) return false
            if (longer.equals(shorter)) return false

            return true
        }

        private fun listFilesNames(dir: File): List<String> {
            val folders = arrayListOf<String>()
            for (file in dir.listFiles()!!) {
                if (file.isDirectory && !file.name.startsWith(".")) folders.add(file.name + "/")
            }

            val files = arrayListOf<String>()
            for (file in dir.listFiles()!!) {
                if (!file.isDirectory && !file.name.startsWith(".")) files.add(file.name)
            }

            return folders.sorted() + files.sorted()
        }

        private fun listFoldersNames(dir: File): List<String> {
            val rz = arrayListOf<String>()

            for (file in dir.listFiles()!!) {
                if (file.isDirectory && !file.name.startsWith(".")) rz.add(file.name)
            }

            return rz.sorted()
        }

    }

}
