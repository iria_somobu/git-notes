package com.somobu.essentials

import android.app.AlertDialog
import android.app.Fragment
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.View
import android.widget.Toast
import java.io.File

abstract class EditorActivity : CustomizedActivity(), ITextEditHost {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHeaderColor(resources.getColor(com.somobu.essentials.R.color.teal))
        setHomeButton(true, {
            hardcodedHome()
            finish()
        })

        val file = getFile()
        if (file == null) {
            Toast.makeText(this, "No file specified", Toast.LENGTH_SHORT).show()
        } else {
            replaceFragment(file)
            updateTitle()
        }
    }

    override fun getBottomButtons(): Array<Btn> {
        return arrayOf(
            object : Btn() {
                override fun getGroup(): String = if (isEditor() && unsaved()) GROUP_DEFAULT else GROUP_NEVER
                override fun getIconRes(): Int = R.drawable.baseline_save_black_36
                override fun getLabelRes(): Int = R.string.btn_save
                override fun click(): (View) -> Unit = { saveFile() }
            },
            object : Btn() {
                override fun getGroup(): String = GROUP_DEFAULT
                override fun getIconRes(): Int =
                    if (isEditor()) R.drawable.baseline_edit_note_black_36 else R.drawable.ic_preview_b

                override fun getLabelRes(): Int = if (isEditor()) R.string.btn_to_viewer else R.string.btn_to_editor
                override fun click(): (View) -> Unit = { toggleEditor() }
            },
            object : Btn() {
                override fun getGroup(): String = GROUP_ALWAYS
                override fun getIconRes(): Int = R.drawable.ic_history
                override fun getLabelRes(): Int = R.string.btn_del_history
                override fun click(): (View) -> Unit = { hardcodedHistory() }
            }
        )
    }

    private fun hardcodedHome() {
        val clazz = Class.forName("com.somobu.gitdesktop.hierarchy.HierarchyActivity")
        val intent = Intent(this, clazz)
        intent.putExtra("file", getFile()?.parentFile?.canonicalPath)
        startActivity(intent)
    }

    private fun hardcodedHistory() {
        val clazz = Class.forName("com.somobu.gitdesktop.viewers.FileHistoryActivity")
        val intent = Intent(this, clazz)
        intent.putExtra("file", getFile()?.canonicalPath)
        startActivity(intent)
    }

    private fun getFile(): File? {
        val file = intent?.getStringExtra("file")

        if (file != null) {
            return File(file).canonicalFile
        }

        if (intent?.data?.scheme == "file") {
            return File(intent?.data?.path).canonicalFile
        }

        return null

    }

    private fun getFragment(): TextEditorFragment? {
        val fragment = getCurrentFragment()
        if (fragment is TextEditorFragment) {
            return fragment
        } else {
            return null
        }
    }

    private fun replaceFragment(file: File) {
        saveFile()

        val fragment =
            if (isEditor()) getTextEditorFragment(file)
            else getViewerFragment(file)

        setFragment(fragment)
    }

    abstract fun getTextEditorFragment(file: File): TextEditorFragment

    abstract fun getViewerFragment(file: File): Fragment

    override fun onBackPressed() {
        val fragment = getFragment()
        if (fragment != null && fragment.isTextChanged()) {
            AlertDialog.Builder(this)
                .setMessage("Save changes?")
                .setNeutralButton("Back", null)
                .setNegativeButton("No") { _: DialogInterface, _: Int -> super.onBackPressed() }
                .setPositiveButton("Yes") { _: DialogInterface, _: Int ->
                    saveFile()
                    super.onBackPressed()
                }
                .show()
        } else {
            super.onBackPressed()
        }
    }

    override fun onTextChanged() {
        updateTitle()
        refreshBottomButtons()
    }

    private fun updateTitle() {
        val textChanged = getFragment()?.isTextChanged()
        val suffix = if (textChanged != null && textChanged) "*" else ""
        setHeaderTitle(getFile()?.name + suffix)
        setHeaderSubtitle(null)
    }

    private fun saveFile() {
        val fragment = getFragment()

        if (unsaved()) {
            getFile()?.writeText(fragment!!.getText())
            fragment!!.markSaved()
            updateTitle()
            Toast.makeText(this, "Saved!", Toast.LENGTH_SHORT).show()

            val intent = Intent()
            intent.action = "com.somobu.gitdesktop.FILE_CHANGED"
            intent.putExtra("data", getFile()!!.absolutePath)
            sendBroadcast(intent)
        }

        refreshBottomButtons()
    }

    fun toggleEditor() {
        PreferenceManager.getDefaultSharedPreferences(this)
            .edit().putBoolean("EDIT_MODE", !isEditor()).apply()
        invalidateOptionsMenu()

        replaceFragment(getFile()!!)
        refreshBottomButtons()
    }

    private fun unsaved(): Boolean {
        return getFragment()?.isTextChanged() ?: false
    }

    private fun isEditor(): Boolean {
        return PreferenceManager.getDefaultSharedPreferences(this).getBoolean("EDIT_MODE", true)
    }

}