import android.app.PendingIntent
import android.os.Build

object Bugs {

    fun piMutable(): Int {
        return if (Build.VERSION.SDK_INT < Build.VERSION_CODES.S) 0
        else PendingIntent.FLAG_MUTABLE
    }

    fun piImmutable(): Int {
        return if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) 0
        else PendingIntent.FLAG_IMMUTABLE
    }
    
}