package com.somobu.widget.todo

import android.appwidget.AppWidgetManager
import android.content.Context
import android.content.Intent
import android.widget.RemoteViews
import android.widget.RemoteViewsService
import java.util.regex.Pattern


class WidgetUpdaterService : RemoteViewsService() {

    override fun onGetViewFactory(intent: Intent): RemoteViewsFactory {
        return TaskListViewsFactory(this, intent)
    }

    class TaskListViewsFactory(val context: Context, val intent: Intent) : RemoteViewsFactory {

        private var rawTitles = arrayListOf<String>()
        private var clearTitles = arrayListOf<String>()

        private val widgetId = intent.getIntExtra(
            AppWidgetManager.EXTRA_APPWIDGET_ID,
            AppWidgetManager.INVALID_APPWIDGET_ID
        )

        override fun onCreate() {
            reload()
        }

        override fun onDestroy() {}

        override fun onDataSetChanged() {
            reload()
        }

        private fun reload() {
            rawTitles.clear()
            clearTitles.clear()

            val file = WidgetProvider.getWidgetFile(context, widgetId)
            if (file != null) {
                val pattern = Pattern.compile("^\\s*#[^#].*\$", Pattern.MULTILINE)
                val matcher = pattern.matcher(file.readText())

                while (matcher.find()) {
                    val rawString = matcher.group()
                    rawTitles.add(rawString.trim())
                    clearTitles.add(rawString.trim().substring(1).trim())
                }
            }
        }

        override fun getViewAt(position: Int): RemoteViews {
            val row = RemoteViews(context.packageName, R.layout.wdg_todo_item)
            row.setTextViewText(android.R.id.title, clearTitles[position])

            val i = Intent()
            i.putExtra("para", rawTitles[position])
            row.setOnClickFillInIntent(R.id.widget_list_item, i)

            return row
        }

        override fun getCount(): Int = clearTitles.size

        override fun getLoadingView(): RemoteViews? = null

        override fun getViewTypeCount(): Int = 1

        override fun getItemId(position: Int): Long = position + 0L

        override fun hasStableIds(): Boolean = true

    }
}