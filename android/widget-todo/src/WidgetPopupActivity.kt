package com.somobu.widget.todo

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.ImageButton
import android.widget.TextView
import io.noties.markwon.Markwon
import io.noties.markwon.ext.strikethrough.StrikethroughPlugin
import io.noties.markwon.ext.tasklist.TaskListPlugin
import java.io.File
import java.util.regex.Pattern

class WidgetPopupActivity : Activity() {

    private var markwon: Markwon? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.wdg_todo_popup)

        markwon = Markwon.builder(this)
            .usePlugin(TaskListPlugin.create(this))
            .usePlugin(StrikethroughPlugin.create())
            .build()

        val textView = findViewById<TextView>(R.id.textView)
        markwon!!.setMarkdown(textView, getCroppedText(getFile()!!, getPara()!!))

        findViewById<ImageButton>(R.id.imageButton).setOnClickListener({
            // TODO: de-hardcode me!
            val clazz = Class.forName("com.somobu.viewtxt.TextEditorActivity")
            val intent = Intent(this, clazz)
            intent.putExtra("file", getFile()!!.absolutePath)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_MULTIPLE_TASK
            startActivity(intent)
            finish()
        })
    }

    private fun getFile(): File? {
        val file = intent?.getStringExtra("file")
        if (file == null) {
            return null
        } else {
            return File(file)
        }
    }

    private fun getPara(): String? {
        return intent?.getStringExtra("para")
    }

    private fun getCroppedText(file: File, para: String): String {
        var text = file.readText()
        val idx = text.indexOf(para)
        if (idx >= 0) text = text.substring(idx)

        val pattern = Pattern.compile("^\\s*#[^#].*", Pattern.MULTILINE)
        val matcher = pattern.matcher(text).region(para.length, text.length)
        if (matcher.find()) text = text.substring(0, matcher.start())

        return text
    }
}