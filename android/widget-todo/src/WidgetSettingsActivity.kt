package com.somobu.widget.todo

import GitO
import android.app.Activity
import android.appwidget.AppWidgetManager
import android.content.Intent
import android.os.Bundle
import com.somobu.essentials.FileChooser
import java.io.File

class WidgetSettingsActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val widgetId = intent?.extras?.getInt(
            AppWidgetManager.EXTRA_APPWIDGET_ID,
            AppWidgetManager.INVALID_APPWIDGET_ID
        ) ?: AppWidgetManager.INVALID_APPWIDGET_ID

        val appWidgetManager = AppWidgetManager.getInstance(this)
        WidgetProvider().onUpdate(this, appWidgetManager, intArrayOf(widgetId))

        val resultValue = Intent().putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId)
        setResult(RESULT_CANCELED, resultValue)

        FileChooser.pickFile(this, "Pick a file: ", GitO.getRepoDir(this), { file: File ->
            run {
                WidgetProvider.setWidgetFile(this, widgetId, file)

                setResult(RESULT_OK, resultValue)
                finish()

                WidgetProvider.invalidateWidgets(this)
            }
        }, {
            run {
                finish()
            }
        })
    }

}