package com.somobu.widget.todo

import Bugs
import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.preference.PreferenceManager
import android.widget.RemoteViews
import java.io.File

@Suppress("DEPRECATION")
class WidgetProvider : AppWidgetProvider() {

    companion object {

        fun setWidgetFile(ctx: Context, widget: Int, file: File) {
            PreferenceManager
                .getDefaultSharedPreferences(ctx)
                .edit()
                .putString("WIDGET_${widget}_FILE", file.absolutePath)
                .apply()
        }

        fun getWidgetFile(ctx: Context, widget: Int): File? {
            val path = PreferenceManager
                .getDefaultSharedPreferences(ctx)
                .getString("WIDGET_${widget}_FILE", null)
            return if (path != null) File(path) else null
        }

        fun invalidateWidgets(ctx: Context) {
            val ids: IntArray = AppWidgetManager
                .getInstance(ctx)
                .getAppWidgetIds(ComponentName(ctx, WidgetProvider::class.java))
                .filter { getWidgetFile(ctx, it) != null }
                .toIntArray()
            
            AppWidgetManager.getInstance(ctx).notifyAppWidgetViewDataChanged(ids, R.id.task_list_widget_lv)

            val intent = Intent(ctx, WidgetProvider::class.java)
            intent.action = AppWidgetManager.ACTION_APPWIDGET_UPDATE
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids)
            ctx.sendBroadcast(intent)
        }
    }

    override fun onUpdate(
        context: Context,
        widgetManager: AppWidgetManager,
        widgetIds: IntArray
    ) {

        for (widgetId in widgetIds) {
            val widget = RemoteViews(context.packageName, R.layout.wdg_todo_main)
            val widgetFile = getWidgetFile(context, widgetId)

            if (widgetFile != null) {
                val clazz = Class.forName("com.somobu.viewtxt.TextEditorActivity")
                val headerIntent = Intent(context, clazz)
                headerIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                headerIntent.action = "" + System.currentTimeMillis()
                headerIntent.putExtra("file", widgetFile.absolutePath)
                val headerPending = PendingIntent.getActivity(
                    context,
                    0,
                    headerIntent,
                    Bugs.piMutable()
                )
                widget.setOnClickPendingIntent(android.R.id.button1, headerPending)
                widget.setTextViewText(android.R.id.button1, widgetFile.name)


                val itemIntent = Intent(context, WidgetPopupActivity::class.java)
                itemIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                itemIntent.action = "" + System.currentTimeMillis()
                itemIntent.putExtra("file", widgetFile.absolutePath)

                val itemPending = PendingIntent.getActivity(
                    context,
                    0,
                    itemIntent,
                    Bugs.piMutable() or PendingIntent.FLAG_UPDATE_CURRENT
                )

                val remoteSvcIntent = Intent(context, WidgetUpdaterService::class.java)
                remoteSvcIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId)
                remoteSvcIntent.data = Uri.parse(remoteSvcIntent.toUri(Intent.URI_INTENT_SCHEME))
                widget.setRemoteAdapter(R.id.task_list_widget_lv, remoteSvcIntent)
                widget.setPendingIntentTemplate(R.id.task_list_widget_lv, itemPending)

                widgetManager.updateAppWidget(widgetId, widget)
            }
        }
    }
}