package com.somobu.widget.note

import Bugs
import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.res.Configuration.ORIENTATION_PORTRAIT
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.widget.RemoteViews
import android.widget.TextView
import io.noties.markwon.Markwon
import io.noties.markwon.ext.strikethrough.StrikethroughPlugin
import io.noties.markwon.ext.tasklist.TaskListPlugin
import java.io.File
import java.lang.Exception
import java.util.regex.Pattern

@Suppress("DEPRECATION")
class WidgetProvider : AppWidgetProvider() {

    companion object {

        fun invalidateWidgets(ctx: Context) {
            val ids: IntArray = AppWidgetManager
                .getInstance(ctx)
                .getAppWidgetIds(ComponentName(ctx, WidgetProvider::class.java))
//                .filter { getWidgetPara(ctx, it) != null }

            AppWidgetManager.getInstance(ctx).notifyAppWidgetViewDataChanged(ids, android.R.id.title)

            val intent = Intent(ctx, WidgetProvider::class.java)
            intent.action = AppWidgetManager.ACTION_APPWIDGET_UPDATE
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids)
            ctx.sendBroadcast(intent)
        }
    }

    override fun onUpdate(c: Context, m: AppWidgetManager, w: IntArray) {
        for (widgetId in w) {
            updateWidget(c, m, widgetId)
        }
    }

    override fun onAppWidgetOptionsChanged(c: Context?, m: AppWidgetManager?, i: Int, newOptions: Bundle?) {
        updateWidget(c!!, m!!, i)
    }

    private fun updateWidget(
        context: Context,
        widgetManager: AppWidgetManager,
        widgetId: Int,
    ) {
        val markwon = Markwon.builder(context)
            .usePlugin(TaskListPlugin.create(context))
            .usePlugin(StrikethroughPlugin.create())
            .build()

        val widget = RemoteViews(context.packageName, R.layout.wdg_note_image)
        val props = WidgetProps.getWidgetProps(context, widgetId)

        val headerIntent = Intent(context, WidgetPopupActivity::class.java)
        headerIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
        headerIntent.action = "" + System.currentTimeMillis()
        headerIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId)

        val headerPending = PendingIntent.getActivity(
            context,
            0,
            headerIntent,
            Bugs.piMutable()
        )
        widget.setOnClickPendingIntent(R.id.image, headerPending)

        val view = LayoutInflater.from(context).inflate(R.layout.wdg_note_main, null, false);
        try {
            val text = props.readSection()
            markwon.setMarkdown(view.findViewById(android.R.id.title), text)
        } catch (e: Exception) {
            view.findViewById<TextView>(android.R.id.title).text = e.message
        }

        val size = getWidgetsSize(context, widgetManager, widgetId)
        val bitmap = Bitmap.createBitmap(
            if (size.first > 0) size.first else 1,
            if (size.second > 0) size.second else 1,
            Bitmap.Config.ARGB_8888
        )

        val canvas = Canvas(bitmap)

        val paint = Paint()
        paint.color = Color.TRANSPARENT
        canvas.drawLine(0.0f, 0.0f, size.first * 1.0f, size.second * 1.0f, paint)

        view.measure(size.first, size.second)
        view.layout(0, 0, size.first, size.second)
        view.draw(canvas)

        widget.setImageViewBitmap(R.id.image, bitmap)
        widgetManager.updateAppWidget(widgetId, widget)

        bitmap.recycle()
    }

    private fun getWidgetsSize(context: Context, appWidgetManager: AppWidgetManager, widgetId: Int): Pair<Int, Int> {
        val isPortrait = context.resources.configuration.orientation == ORIENTATION_PORTRAIT
        val width = getWidgetWidth(appWidgetManager, isPortrait, widgetId)
        val height = getWidgetHeight(appWidgetManager, isPortrait, widgetId)
        val widthInPx = context.dip(width)
        val heightInPx = context.dip(height)
        return widthInPx to heightInPx
    }

    private fun getWidgetWidth(appWidgetManager: AppWidgetManager, isPortrait: Boolean, widgetId: Int): Int =
        if (isPortrait) {
            getWidgetSizeInDp(appWidgetManager, widgetId, AppWidgetManager.OPTION_APPWIDGET_MIN_WIDTH)
        } else {
            getWidgetSizeInDp(appWidgetManager, widgetId, AppWidgetManager.OPTION_APPWIDGET_MAX_WIDTH)
        }

    private fun getWidgetHeight(appWidgetManager: AppWidgetManager, isPortrait: Boolean, widgetId: Int): Int =
        if (isPortrait) {
            getWidgetSizeInDp(appWidgetManager, widgetId, AppWidgetManager.OPTION_APPWIDGET_MAX_HEIGHT)
        } else {
            getWidgetSizeInDp(appWidgetManager, widgetId, AppWidgetManager.OPTION_APPWIDGET_MIN_HEIGHT)
        }

    private fun getWidgetSizeInDp(appWidgetManager: AppWidgetManager, widgetId: Int, key: String): Int {
        return appWidgetManager.getAppWidgetOptions(widgetId).getInt(key, 0)
    }

    private fun Context.dip(value: Int): Int {
        return (value * resources.displayMetrics.density).toInt()
    }

}