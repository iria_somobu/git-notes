package com.somobu.widget.note

import GitO
import android.content.Context
import com.google.gson.Gson
import java.io.File
import java.lang.Exception
import java.util.regex.Pattern

class WidgetProps(
    var file: String,
    var para: String,
) {

    companion object {

        const val NEW_SECTION = "new-section"
        const val TOP_SECTION = "top-section"

        fun getWidgetProps(ctx: Context, widgetId: Int): WidgetProps {
            try {
                return Gson()
                    .fromJson(
                        File(GitO.getRepoDir(ctx), "../widgets/wdg-note-$widgetId.json").readText(),
                        WidgetProps::class.java
                    )!!
            } catch (e: Exception) {
                return WidgetProps(File(GitO.getRepoDir(ctx), "notes.md").absolutePath, NEW_SECTION);
            }
        }

        fun writeWidgetProps(ctx: Context, widgetId: Int, props: WidgetProps) {
            assert(props.para != NEW_SECTION)

            val file = File(GitO.getRepoDir(ctx), "../widgets/wdg-note-$widgetId.json")
            file.parentFile.mkdirs()
            file.writeText(Gson().toJson(props))
        }
    }

    fun getFile(): File {
        return File(file)
    }

    fun isHardcodedSection(): Boolean {
        return para == NEW_SECTION || para == TOP_SECTION
    }

    fun readSections(): List<String> {
        val list = ArrayList<String>()

        val text: String;
        try {
            text = getFile().readText().trim()
        } catch (e: Exception) {
            return emptyList()
        }

        // This regex looks for top-level markdown header, "# Smth"
        val pattern = Pattern.compile("^\\h*#[^#].*$", Pattern.MULTILINE)
        val matcher = pattern.matcher(text)

        while (matcher.find()) {
            list.add(text.substring(matcher.start(), matcher.end()))
        }

        return list;
    }

    fun readSection(includeHeader: Boolean = false): String {
        if (para == NEW_SECTION) {
            return ""
        }

        var text = getFile().readText()

        // This regex looks for top-level markdown header, "# Smth"
        val pattern = Pattern.compile("^\\h*#[^#].*$", Pattern.MULTILINE)

        if (para == TOP_SECTION) {
            val matcher = pattern.matcher(text)

            var from = 0
            if (matcher.find()) {
                if (includeHeader) {
                    from = matcher.start()
                } else {
                    from = matcher.end() + 1 // +1 is to strip newline
                }
            }

            var to = text.length
            if (matcher.find()) to = matcher.start()

            return text.substring(from, to)

        } else {
            val idx = text.indexOf(para)

            if (idx < 0) throw Exception("Unable to locate paragraph \"$para\"")

            text = text.substring(idx)

            val matcher = pattern.matcher(text).region(para.length, text.length)
            if (matcher.find()) text = text.substring(0, matcher.start())
            
            if (!includeHeader) {
                text = text.substring(text.indexOf("\n") + 1)
            }

            return text
        }
    }

    override fun toString(): String {
        return "$file $para"
    }
}

