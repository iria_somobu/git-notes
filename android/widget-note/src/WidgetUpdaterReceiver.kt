package com.somobu.widget.note

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class WidgetUpdaterReceiver : BroadcastReceiver() {

    override fun onReceive(ctx: Context, p1: Intent) {
        WidgetProvider.invalidateWidgets(ctx)
    }

}