package com.somobu.widget.note

import GitO
import android.app.Activity
import android.appwidget.AppWidgetManager
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import com.somobu.essentials.FileChooser
import io.noties.markwon.Markwon
import io.noties.markwon.editor.MarkwonEditor
import io.noties.markwon.editor.MarkwonEditorTextWatcher
import io.noties.markwon.ext.tasklist.TaskListPlugin
import java.io.File
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.Executors

class WidgetPopupActivity : Activity() {

    lateinit var props: WidgetProps
    lateinit var markwon: Markwon

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.wdg_note_popup)

        val resultValue = Intent().putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, getWidgetId())
        setResult(RESULT_OK, resultValue)

        props = WidgetProps.getWidgetProps(this, getWidgetId())

        val fileSelectButton = findViewById<Button>(R.id.button)
        fileSelectButton.text = props.getFile().name
        fileSelectButton.setOnClickListener({
            FileChooser.pickFile(this, "Pick a file: ", GitO.getRepoDir(this), { file: File ->
                run {
                    props.file = file.absolutePath
                    props.para = WidgetProps.NEW_SECTION
                    fileSelectButton.text = file.name
                    reloadAdapter()
                    reloadTextView()
                }
            }, {})
        })


        val spinnerAdapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item)

        val spinner = findViewById<Spinner>(R.id.spinner)
        spinner.adapter = spinnerAdapter
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, i: Int, p3: Long) {
                props.para = when (i) {
                    0 -> WidgetProps.NEW_SECTION
                    1 -> WidgetProps.TOP_SECTION
                    else -> {
                        val sections = props.readSections()
                        sections[i - 2]
                    }
                }

                reloadTextView()
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

        }
        reloadAdapter()


        markwon = Markwon.builder(this)
            .usePlugin(TaskListPlugin.create(this))
            .build()
        val editor = MarkwonEditor.create(markwon)
        val editText = findViewById<EditText>(R.id.wdg_note_popup_text)
        editText.addTextChangedListener(
            MarkwonEditorTextWatcher.withPreRender(
                editor,
                Executors.newCachedThreadPool(),
                editText
            )
        )

        reloadTextView()
    }

    private fun getWidgetId(): Int {
        return intent?.extras?.getInt(
            AppWidgetManager.EXTRA_APPWIDGET_ID,
            AppWidgetManager.INVALID_APPWIDGET_ID
        ) ?: AppWidgetManager.INVALID_APPWIDGET_ID
    }

    private fun reloadAdapter() {
        val spinner = findViewById<Spinner>(R.id.spinner)
        val spinnerAdapter = spinner.adapter as ArrayAdapter<String>
        spinnerAdapter.clear()
        spinnerAdapter.add("[New section]")
        spinnerAdapter.add("[Topmost section]")

        val sections = props.readSections()

        var currentSection = -1

        var idx = 0
        for (section in sections) {
            if (section == props.para) {
                currentSection = idx
            }

            var sub = section.substring(1)
            sub = sub.trim()

            spinnerAdapter.add(sub)

            idx += 1
        }

        if (props.para == WidgetProps.NEW_SECTION) {
            spinner.setSelection(0)
        } else if (props.para == WidgetProps.TOP_SECTION) {
            spinner.setSelection(1)
        } else {
            spinner.setSelection(2 + currentSection)
        }
    }

    private fun reloadTextView() {
        try {
            val text = props.readSection().trim()

            val textView = findViewById<EditText>(R.id.wdg_note_popup_text)
            textView.setText(text)
        } catch (e: Exception) {
            Toast.makeText(this, e.message, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onPause() {
        super.onPause()

        val resultValue = Intent().putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, getWidgetId())

        val targetFile = props.getFile()
        if (!targetFile.exists()) {
            targetFile.parentFile.mkdirs()
            targetFile.createNewFile()
        }

        val textView = findViewById<EditText>(R.id.wdg_note_popup_text)
        val text = textView.text.toString().trim()

        if (props.readSection().trim() != text) {

            if (props.para == WidgetProps.NEW_SECTION) {
                val time = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Date())
                val header = "# $time"
                targetFile.appendText("\n\n$header\n$text\n")

                props.para = header
            } else {
                var header = props.para

                if (header == WidgetProps.TOP_SECTION) {
                    header = props.readSections()[0]
                }

                val originalText = props.readSection(true)
                val alteredText = "$header\n$text\n\n\n"

                var completeText = targetFile.readText()
                completeText = completeText.replace(originalText, alteredText)

                targetFile.writeText(completeText)
            }

        }

        WidgetProps.writeWidgetProps(this, getWidgetId(), props)
        setResult(RESULT_OK, resultValue)
        WidgetProvider.invalidateWidgets(this)
    }

}
