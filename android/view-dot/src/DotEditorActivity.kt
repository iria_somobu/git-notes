package com.somobu.viewdot

import android.app.Fragment
import com.somobu.essentials.EditorActivity
import com.somobu.essentials.TextEditorFragment
import java.io.File

class DotEditorActivity : EditorActivity() {

    override fun getTextEditorFragment(file: File): TextEditorFragment {
        return DotEditorFragment.instance(file)
    }

    override fun getViewerFragment(file: File): Fragment {
        return DotViewerFragment.instance(file)
    }

}
