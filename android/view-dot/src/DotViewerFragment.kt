package com.somobu.viewdot

import android.annotation.SuppressLint
import android.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.ScrollView
import android.widget.TextView
import com.somobu.essentials.CustomizedActivity
import com.somobu.essentials.EditorActivity
import java.io.File

class DotViewerFragment : Fragment() {

    companion object StaticMethods {

        fun instance(file: File): DotViewerFragment {
            val f = DotViewerFragment()

            val args = Bundle()
            args.putString("FILE", file.absolutePath)
            f.arguments = args

            return f
        }
    }

    private fun getFile(): File {
        return File(arguments.getString("FILE")!!)
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_dot_view, container, false)
        val webView = view.findViewById<ObservableWebView>(R.id.webview)

        val text = getFile().readText()
        val empty = text.trim().isEmpty()

        webView.visibility = if (empty) View.GONE else View.VISIBLE
        view.findViewById<LinearLayout>(R.id.contentless).visibility = if (empty) View.VISIBLE else View.GONE

        webView.settings.javaScriptEnabled = true
        webView.settings.builtInZoomControls = true
        webView.settings.displayZoomControls = false
        webView.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView?, url: String?) {
                var streeng = text
                // This is fucking stupid and non-optimal but fuck it
                // This is also huge fucking hole in fucking "security"
                // I tired of this fucking crappy shit Android is
                // Redo it better if you can or fuck off
                streeng = streeng
                    .replace("\n", "")
                    .replace("\"", "\\\"")
                view!!.evaluateJavascript("viewdot(\"$streeng\");", null);
            }
        }

        webView.loadUrl("file:///android_asset/index.html")

        view.findViewById<ImageButton>(R.id.btn_switch_to_edit).setOnClickListener({
            run {
                if (activity is EditorActivity) {
                    val activity = activity as EditorActivity
                    activity.toggleEditor()
                }
            }
        })


        webView.onScrollChangedCallback = object : ObservableWebView.OnScrollChangedCallback {

            var prevScroll: Int = 0

            override fun onScroll(l: Int, t: Int, oldl: Int, oldt: Int) {
                val currentScroll = webView.scrollY
                val maActivity = this@DotViewerFragment.activity as CustomizedActivity?
                maActivity?.handleScrollY((prevScroll - currentScroll).toFloat())
                prevScroll = currentScroll
            }
        }

        return view
    }

}